#include "rbf_interpolate.hpp"

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#ifdef __linux
#include <CL/cl.h>
#else
#ifdef WIN32
#include <CL/cl.h>
#endif //WIN32
#endif //__linux
#endif //_APPLE

#include "cl.hpp"



#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <functional>
#include <cmath>
#include <sstream>
#include <utility>
#include <assert.h>
#include <numeric>
#include <limits>
#include <sys/time.h>


#define CL_CHECK_CASE(ERROR_CODE)    \
case ERROR_CODE: {                            \
	message = #ERROR_CODE;                    \
	std::cerr << "[Error] " << message << std::endl;        \
	return false;                            \
}

namespace rbf_pum
{

	namespace
	{

		bool check_cl_error(const cl_int code) {
			std::string message;

			switch (code) {
				case CL_SUCCESS: {
					message = "";
					return true;
				}

				CL_CHECK_CASE(CL_INVALID_PLATFORM)
				CL_CHECK_CASE(CL_INVALID_DEVICE_TYPE)
				CL_CHECK_CASE(CL_INVALID_VALUE)
				CL_CHECK_CASE(CL_DEVICE_NOT_FOUND)
				CL_CHECK_CASE(CL_INVALID_COMMAND_QUEUE)
				CL_CHECK_CASE(CL_INVALID_OPERATION)
				CL_CHECK_CASE(CL_COMPILER_NOT_AVAILABLE)
				CL_CHECK_CASE(CL_OUT_OF_HOST_MEMORY)
				CL_CHECK_CASE(CL_BUILD_PROGRAM_FAILURE)
				CL_CHECK_CASE(CL_INVALID_DEVICE)
				CL_CHECK_CASE(CL_INVALID_HOST_PTR);

//clSetKernelArg
				CL_CHECK_CASE(CL_INVALID_KERNEL)
				CL_CHECK_CASE(CL_INVALID_ARG_INDEX)
				CL_CHECK_CASE(CL_INVALID_ARG_VALUE)
				CL_CHECK_CASE(CL_INVALID_MEM_OBJECT)
				CL_CHECK_CASE(CL_INVALID_ARG_SIZE)
				CL_CHECK_CASE(CL_INVALID_SAMPLER)


				CL_CHECK_CASE(CL_MAP_FAILURE);
				CL_CHECK_CASE(CL_INVALID_GLOBAL_WORK_SIZE);

				CL_CHECK_CASE(CL_INVALID_GL_OBJECT)
				CL_CHECK_CASE(CL_INVALID_CONTEXT)
				CL_CHECK_CASE(CL_INVALID_QUEUE_PROPERTIES)
				CL_CHECK_CASE(CL_INVALID_KERNEL_NAME)
				CL_CHECK_CASE(CL_INVALID_KERNEL_DEFINITION)
				CL_CHECK_CASE(CL_INVALID_EVENT_WAIT_LIST)
				CL_CHECK_CASE(CL_INVALID_WORK_DIMENSION)
				CL_CHECK_CASE(CL_INVALID_WORK_GROUP_SIZE)
				CL_CHECK_CASE(CL_INVALID_WORK_ITEM_SIZE)
				CL_CHECK_CASE(CL_MEM_OBJECT_ALLOCATION_FAILURE)
				CL_CHECK_CASE(CL_INVALID_KERNEL_ARGS)

				default: {
					std::cerr << "unknown return value for " << code << std::endl;
					return false;
				}
			}

			return false;
		}


#undef CL_CHECK_CASE


		enum RBFType {
			GA = 0,
			IMQ,
			M2,
			M4,
			M6,
			W2,
			W4,
			W6,
			INVALID
		};

		inline int rbf_type_from_string(const std::string &str) {
			if(str == "GA")  { return GA; }

			if(str == "IMQ") { return IMQ; }

			if(str == "M2")  { return M2; }
			if(str == "M4")  { return M4; }
			if(str == "M6")  { return M6; }

			if(str == "W2")  { return W2; }
			if(str == "W4")  { return W4; }
			if(str == "W6")  { return W6; }

			return INVALID;
		}

		inline Scalar get_preferred_epsilon(const int rbf_type){
			static const Scalar eps[] =
			{
				2.0,                              //GA
				2.0,                              //IMQ
				2.0,  2.0,  2.0,                  //Mx
				0.01, 0.01, 0.01                  //Wx
			};

			return eps[rbf_type];
		}

		void error_usage() {
			std::cerr << "[Error] Wrong number of parameters\n" << std::endl;
		}

		void print_usage() {
			std::cout << "usage: ./rbf_interpolate <points.txt> <function.txt> <eval_points.txt> <interpolated_function.txt>\n";
			std::cout << "Optionals:";
			std::cout << "\n\t-r <The type of rbf = M4> \n\t-p <number of thrad used = -1> \n\t-u <true if is [0, 1]^dim = 0> \n\t-o <oracle path = \'\'>\n\t-t <optimize local epsilons = 1>\n" << std::endl;
		}


		template<typename T>
		void print_vec(const std::vector<T> &vec, std::ostream &os)
		{
			for(typename std::vector<T>::const_iterator it = vec.begin(); it != vec.end(); ++it) {
				os << *it << "\n";
			}
		}

		bool read_vec(const std::string &path, const char separator, std::vector<Scalar> &vec)
		{
			std::ifstream ss;
			ss.open(path.c_str());

			if(!ss.good()) {
				std::cerr << "[Error] unable to read file: " << path << std::endl;
				return false;
			}

			std::string line, num;
			while(ss.good()) {
				std::getline(ss, line);

				std::istringstream linestream(line);
				while(std::getline(linestream, num, separator)) {
					vec.push_back(atof(num.c_str()));
				}
			}

			ss.close();
			return true;
		}

		bool read_string(const std::string &path, std::string &str)
		{
#ifdef RBF_SCRIPT_ROOT
			std::string root = RBF_SCRIPT_ROOT;
#else
			std::string root = "";
#endif
			std::stringstream content;
			std::string line;
			std::ifstream file ((root + path).c_str());

			if (file.is_open())
			{
				while ( file.good() )
				{
					getline (file,line);
					content << line << "\n";
				}
				file.close();
			}
			else {

				std::cerr<<"Unable the open "<<path<<std::endl;
				return false;
			}

			str = content.str();
			return true;
		}


		class Chrono {
		public:

			void tick()
			{
				gettimeofday(&begin_, NULL);
			}

			void tock()
			{
				gettimeofday(&end_, NULL);

				const double startTimeInMicroSec = (begin_.tv_sec * 1000000.0) + begin_.tv_usec;
				const double endTimeInMicroSec = (end_.tv_sec * 1000000.0) + end_.tv_usec;
				elapsed_ = (endTimeInMicroSec - startTimeInMicroSec)*1e-6;
			}

			inline double elapsed() const
			{
				return elapsed_;
			}

			void print(std::ostream &os = std::cout) const
			{
				os << elapsed() << " seconds";
			}

			friend std::ostream &operator<<(std::ostream &os, const Chrono &c)
			{
				c.print(os);
				return os;
			}

		private:
			timeval begin_, end_;
			double elapsed_;
		};


		class Env {
		public:
			void init()
			{
				cl::Platform::get(&platforms);
				platforms[current_platform].getDevices(CL_DEVICE_TYPE_ALL, &devices);
        		// platforms[current_platform].getDevices(CL_DEVICE_TYPE_GPU, &devices); current_device = 1;

				std::vector<cl::Device> dev(1);
				dev[0]  = devices[current_device];
				context = cl::Context(dev);
				queue   = cl::CommandQueue(context, devices[current_device]);
			}

			cl::Context &get_context()
			{
				return context;
			}

			cl::CommandQueue &get_queue()
			{
				return queue;
			}

			cl::Device &get_device()
			{
				return devices[current_device];
			}

			void describe_current_setup() const
			{
				std::cout << "Using platform: " << platforms[current_platform].getInfo<CL_PLATFORM_NAME>() << "\n";
				std::cout << "  Using device: " << devices[current_device].getInfo<CL_DEVICE_VENDOR>() << " ";
				std::cout << devices[current_device].getInfo<CL_DEVICE_NAME>() << " ";
				std::cout << "(" << devices[current_device].getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << " compute units)\n";
			}

			Env()
			: current_platform(0), current_device(0)
			{}

			inline bool compile_program(const std::string &code, const std::string &flags, cl::Program &program)
			{
				cl::Program::Sources sources;
				sources.push_back( std::make_pair(code.c_str(), size_t(code.length())) );

				program = cl::Program(context, sources);
				std::vector<cl::Device> dev(1);
				dev[0] = devices[current_device];

				if(program.build(dev, flags.c_str()) != CL_SUCCESS) {
					std::cerr<< " Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>( devices[current_device] ) << "\n";
					return false;
				}

				return true;
			}


		private:
			std::vector<cl::Platform> platforms;
			std::vector<cl::Device> devices;
			cl::Context context;
			cl::CommandQueue queue;
			int current_platform, current_device;
		};


		template<typename T>
		class CLVector {
		public:
			std::vector<T> entries;
			cl::Buffer buffer;
			Env &env_;

			CLVector(Env &env, const SizeType n_entries = 0)
			: env_(env)
			{
				if(n_entries > 0) {
					resize(n_entries);
				}
			}

			inline SizeType size() const
			{
				return entries.size();
			}

			bool init_buffer()
			{
				assert(!entries.empty());

				cl_int err;
				buffer = cl::Buffer(env_.get_context(),
					CL_MEM_USE_HOST_PTR, sizeof(T) * entries.size(),
					&entries[0],
					&err
					);

				assert(check_cl_error(err));
				return CL_SUCCESS == err;
			}

			bool resize(const SizeType n_entries)
			{
				if(n_entries != entries.size()) {
					entries.reserve(n_entries);
					entries.resize(n_entries);

					return init_buffer();
				}

				return true;
			}

			inline bool synch_read_buffer()
			{
				cl_int err = env_.get_queue().enqueueReadBuffer(buffer, CL_TRUE, 0, sizeof(T) * entries.size(), &entries[0]);
				assert(check_cl_error(err));
				return CL_SUCCESS == err;
			}

			inline bool synch_write_buffer()
			{
				cl_int err = env_.get_queue().enqueueWriteBuffer(buffer, CL_TRUE, 0, sizeof(T) * entries.size(), &entries[0]);
				assert(check_cl_error(err));
				return CL_SUCCESS == err;
			}
		};

		typedef CLVector<Scalar>   RealVector;
		typedef CLVector<SizeType> IntVector;

} //anonymous namespace


int init(const std::vector<Scalar> &points_v, const std::vector<Scalar> &function_v, RBFData &data,
	int verbose,
	const std::string &rbf,
	bool opt,
	bool unit_cube,
	int num_threads
	)
{
	Scalar tol = 0;
	std::string prec_str = "";

	if(sizeof(Scalar) == 4) {
		prec_str = " -DUSE_SINGLE_PRECISION=1";
		tol = 1e-6;
	} else {
		prec_str = " -DUSE_DOUBLE_PRECISION=1";
		tol = 1e-8;
	}

	// create opencl context and queue
	Env env;
	env.init();
	if(verbose > 1)
		env.describe_current_setup();
	cl_int err = CL_SUCCESS;

	RealVector points(env), function(env), eval_points(env);
	points.entries = points_v;
	function.entries = function_v;

	if(!points.init_buffer()      || !points.synch_write_buffer())       return EXIT_FAILURE;
	if(!function.init_buffer()    || !function.synch_write_buffer())     return EXIT_FAILURE;


	const int dim = points.size()/function.size();
	std::stringstream dimss; dimss << dim;
	const std::string dim_str = dimss.str();


	if(verbose > 1)
		std::cout << "\tdim: " << dim << std::endl;

	Chrono script_time, algo_time;
	script_time.tick();

	std::string isect_prg_str, rbf_prg_str;

	// read programs
	if(!read_string("kernels/pum_space_partitioning.cl", isect_prg_str)) return EXIT_FAILURE;
	if(!read_string("kernels/pum_RBF.cl", rbf_prg_str))                  return EXIT_FAILURE;


	std::string opt_eps_str;
	if(opt) {
		opt_eps_str = " -Dmust_find_optimal_epsilon=1";
	} else {
		opt_eps_str = " -Dmust_find_optimal_epsilon=0";
	}

// build programs
	cl::Program isect_prg;
#ifdef RBF_SCRIPT_ROOT
			std::string root = RBF_SCRIPT_ROOT;
#else
			std::string root = "./";
#endif
	if(!env.compile_program(isect_prg_str, "-I" + root + "kernels -DN_DIMS=" + dim_str +  " " + prec_str, isect_prg)) return EXIT_FAILURE;

	if(verbose > 1)
		std::cout << "Using " << rbf << " kernel" <<std::endl;

	const SizeType rbf_type = rbf_type_from_string(rbf);
	const Scalar eps = get_preferred_epsilon(rbf_type);


	const SizeType n_points = points.size()/dim; assert(dim * n_points == points.size());
	const SizeType n_subdomains = ceil(0.5 * pow(n_points * 0.5, 1.0/dim));

	if(verbose > 1)
	{
		std::cout << "Input points: " << n_points <<  std::endl;

		std::cout << "npu: " << n_subdomains << std::endl;
	}

	Scalar total_time = 0;

	cl::Kernel compute_aabbs = cl::Kernel(isect_prg, "compute_aabbs");

    // const cl_ulong max_group_size      = env.get_device().getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
    // const cl_ulong group_size_multiple = compute_aabbs.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(env.get_device());

    // cl_ulong temp = floor(log(cl_ulong(floor(n_points/group_size_multiple)))/log(2));
    // cl_ulong group_size = std::min( std::max(cl_ulong(pow(2, temp)), cl_ulong(1)) * group_size_multiple, max_group_size);

    // if(env.get_device().getInfo<CL_DEVICE_TYPE>() == CL_DEVICE_TYPE_CPU) {
	cl_ulong group_size = 1;
    // }

	cl_ulong n_groups = n_points/group_size;
	cl_ulong global_size = n_groups * group_size;

    // std::cout << "max_work_group_size: " << max_group_size << std::endl;
    // std::cout << "group_size_multiple: " << group_size_multiple << std::endl;

	if(verbose > 1)
	{
		std::cout << "-----------------------------" << std::endl;
		std::cout << "group_size: "  << group_size   << std::endl;
		std::cout << "n_groups: "    << n_groups     << std::endl;
		std::cout << "global_size: " << global_size  << std::endl;
		std::cout << "-----------------------------" << std::endl;
	}
	if(num_threads > 0) {
		group_size = 1;
		global_size = num_threads;
		n_groups = 1;
	}

	RealVector aabb(env);

	if(unit_cube) {
		aabb.entries.resize(dim * 2);
        // fill max coords
		std::fill(aabb.entries.begin(),        aabb.entries.begin() + dim, 0);
		std::fill(aabb.entries.begin() + dim,  aabb.entries.end(), 1);
	} else {
// ####################################################################################
// ####################################################################################
// #############################Compute aabbs##########################################

		algo_time.tick();

		RealVector aabbs(env, n_groups * 2 * dim);


		err = compute_aabbs.setArg(0, n_points);                                              assert(check_cl_error(err));
		err = compute_aabbs.setArg(1, points.buffer);                                         assert(check_cl_error(err));
		err = compute_aabbs.setArg(2, cl::__local(sizeof(Scalar) * group_size * 2 * dim));    assert(check_cl_error(err));
		err = compute_aabbs.setArg(3, aabbs.buffer);                                          assert(check_cl_error(err));

		err = env.get_queue().enqueueNDRangeKernel(compute_aabbs, cl::NullRange, cl::NDRange(global_size), cl::NDRange(group_size)); assert(check_cl_error(err));
        // reads the content for completing the reduction and obtaining aaabb
		aabbs.synch_read_buffer();
		env.get_queue().finish();


		aabb.entries.resize(2 * dim);
		std::copy(aabbs.entries.begin(), aabbs.entries.begin() + (2 * dim), aabb.entries.begin());
		for(int i = 1; i < n_groups; ++i) {
			int offset = i * 2;
			for(int j = 0; j < dim; ++j) {
				aabb.entries[j]     = std::min(aabbs.entries[dim*offset + j], aabb.entries[j]);
				aabb.entries[dim+j] = std::max(aabbs.entries[(offset+1)*dim + j], aabb.entries[dim+j]);

			}
		}

		algo_time.tock();
		total_time += algo_time.elapsed();
		if(verbose > 0)
			std::cout <<  "compute_aabbs: " << algo_time << std::endl;
	}


	std::vector<Scalar> aabb_ranges(dim);
	Scalar aabb_ranges_min = std::numeric_limits<Scalar>::max();

	for(int j = 0; j < dim; ++j) {
		aabb_ranges[j] = aabb.entries[j+dim] - aabb.entries[j];
		aabb_ranges_min = std::min(aabb_ranges_min, aabb_ranges[j]);
	}


	std::vector<SizeType> n_cells_x_dim(dim);
	SizeType total_npu = 1;

	for(int i = 0; i < dim; ++i) {
		n_cells_x_dim[i] = n_subdomains * (aabb_ranges[i]/aabb_ranges_min);
		total_npu *= n_cells_x_dim[i];
	}

	if(verbose > 1)
		std::cout << "total_npu: " << total_npu << std::endl;

	for(int j = 0; j < dim; ++j) {
		aabb.entries[j] -= tol;
		aabb.entries[j + dim] += tol;
	}

	for(int j = 0; j < dim; ++j) {
		const Scalar h = (aabb.entries[dim + j] - aabb.entries[j])/n_cells_x_dim[j];
		aabb.entries[dim + j] += h;
		aabb.entries[j]       -= h;
	}

	IntVector dims(env);
	dims.entries.resize(dim);
	for(int i = 0; i < dim; ++i) {
		dims.entries[i] = n_cells_x_dim[i] + 2;
	}

	aabb.init_buffer();
	aabb.synch_write_buffer();

	dims.init_buffer();
	dims.synch_write_buffer();

// #################################################################
// #################### Find cells #############################

	algo_time.tick();

	IntVector indicator(env, n_points);

	cl::Kernel find_cells = cl::Kernel(isect_prg, "find_cells");

	err = find_cells.setArg(0, n_points);           assert(check_cl_error(err));
	err = find_cells.setArg(1, points.buffer);      assert(check_cl_error(err));
	err = find_cells.setArg(2, aabb.buffer);        assert(check_cl_error(err));
	err = find_cells.setArg(3, dims.buffer);        assert(check_cl_error(err));
	err = find_cells.setArg(4, indicator.buffer);   assert(check_cl_error(err));

	err = env.get_queue().enqueueNDRangeKernel(find_cells, cl::NullRange, cl::NDRange(global_size), cl::NullRange); assert(check_cl_error(err));
	indicator.synch_read_buffer();
	env.get_queue().finish();


	algo_time.tock();
	total_time += algo_time.elapsed();
	if(verbose > 0)
		std::cout << "find_cells: " << algo_time << std::endl;

// #################################################################
// ########################## count points in cells ################

	SizeType n_cells = 1;

	for(SizeType i = 0; i < dims.size(); ++i) {
		n_cells *= dims.entries[i];
	}

	std::vector<SizeType> cell_count(n_cells, 0);

	for(SizeType i = 0; i < indicator.size(); ++i) {
		cell_count[indicator.entries[i]] += 1;
	}


// #################################################################
// ########################## fill cells ###########################

	algo_time.tick();

	IntVector cell_ptr(env);
	cell_ptr.entries.resize(cell_count.size() + 1);
	cell_ptr.entries[0] = 0;

	for(SizeType i = 0; i < cell_count.size(); ++i) {
		cell_ptr.entries[i + 1] = cell_ptr.entries[i] + cell_count[i];
	}

	std::vector<SizeType> inserted(cell_count.size(), 0);

	IntVector cell_index(env);
	cell_index.entries.resize(indicator.size());

	for(SizeType i = 0; i < indicator.size(); ++i) {
		const SizeType ind = indicator.entries[i];
		cell_index.entries[cell_ptr.entries[ind] + inserted[ind]] = i;
		inserted[ind] += 1;
	}

	algo_time.tock();
	total_time += algo_time.elapsed();
	if(verbose > 0)
		std::cout << "fill_cells: " << algo_time << std::endl;

// #################################################################
// #################### rearrange points ###########################

	algo_time.tick();
	cell_ptr.init_buffer();
	cell_ptr.synch_write_buffer();

	cell_index.init_buffer();
	cell_index.synch_write_buffer();

	RealVector rearranged_points(env,   points.size());
	RealVector rearranged_function(env, function.size());

	SizeType n_global = cell_ptr.size() - 1;
	if(num_threads > 0) {
		n_global = num_threads;
	}

	cl::Kernel rearrange_points = cl::Kernel(isect_prg, "rearrange_points");

	err = rearrange_points.setArg(0, dims.buffer);                  assert(check_cl_error(err));
	err = rearrange_points.setArg(1, cell_ptr.buffer);              assert(check_cl_error(err));
	err = rearrange_points.setArg(2, cell_index.buffer);            assert(check_cl_error(err));
	err = rearrange_points.setArg(3, points.buffer);                assert(check_cl_error(err));
	err = rearrange_points.setArg(4, function.buffer);              assert(check_cl_error(err));
	err = rearrange_points.setArg(5, rearranged_points.buffer);     assert(check_cl_error(err));
	err = rearrange_points.setArg(6, rearranged_function.buffer);   assert(check_cl_error(err));

	err = env.get_queue().enqueueNDRangeKernel(rearrange_points, cl::NullRange, cl::NDRange(n_global), cl::NullRange); assert(check_cl_error(err));
	rearranged_points.synch_read_buffer();
	rearranged_function.synch_read_buffer();
	env.get_queue().finish();

	algo_time.tock();
	total_time += algo_time.elapsed();
	if(verbose > 0)
		std::cout << "rearrange points: " << algo_time << std::endl;

// #################################################################
// #################### count points in subdomain ##################

	algo_time.tick();
	Scalar const & (*min) (Scalar const &, Scalar const &) = std::min<Scalar>;
	const Scalar max_radius = sqrt(2)/(std::accumulate(n_cells_x_dim.begin(), n_cells_x_dim.end(), n_cells_x_dim[0], min)) * aabb_ranges_min;

	n_global = n_cells;
	if(num_threads > 0) {
		n_global = num_threads;
	}

	IntVector n_elements(env, n_cells);

	cl::Kernel count_points_in_subdomain = cl::Kernel(isect_prg, "count_points_in_subdomain");

	err = count_points_in_subdomain.setArg(0, cell_ptr.buffer);             assert(check_cl_error(err));
	err = count_points_in_subdomain.setArg(1, cell_index.buffer);           assert(check_cl_error(err));
	err = count_points_in_subdomain.setArg(2, dims.buffer);                 assert(check_cl_error(err));
	err = count_points_in_subdomain.setArg(3, max_radius);                  assert(check_cl_error(err));
	err = count_points_in_subdomain.setArg(4, aabb.buffer);                 assert(check_cl_error(err));
	err = count_points_in_subdomain.setArg(5, rearranged_points.buffer);    assert(check_cl_error(err));
	err = count_points_in_subdomain.setArg(6, n_elements.buffer);           assert(check_cl_error(err));

	err = env.get_queue().enqueueNDRangeKernel(count_points_in_subdomain, cl::NullRange, cl::NDRange(n_global), cl::NullRange); assert(check_cl_error(err));
	n_elements.synch_read_buffer();
	env.get_queue().finish();

	algo_time.tock();
	total_time += algo_time.elapsed();
	if(verbose > 0)
		std::cout << "count_points_in_subdomain: " << algo_time << std::endl;

// // #################################################################
// // #################### compute rbf weights  #######################
	Scalar const & (*max) (Scalar const &, Scalar const &) = std::max<Scalar>;
	const SizeType max_n_dofs = std::accumulate(n_elements.entries.begin(), n_elements.entries.end(), n_elements.entries[0], max);
	if(verbose > 1)
		std::cout << "max_n_dofs: " << max_n_dofs << std::endl;
	assert(max_n_dofs > 0);

	std::stringstream ss;
	ss << max_n_dofs;
	std::string rbf_flags = " -DMAX_N_DOFS="+ss.str() + opt_eps_str;

	const bool use_dynamic_mem = dim > 2;

	if(use_dynamic_mem) {
		rbf_flags += " -DUSE_GLOBAL_MATRIX ";
	}

	std::string flags = "-I" + root + "kernels -DN_DIMS=" + dim_str +  " " + prec_str + rbf_flags;

    // build programs
	cl::Program rbf_prg;
	if(!env.compile_program(rbf_prg_str, flags, rbf_prg)) return EXIT_FAILURE;

	if(verbose > 1)
		std::cout << "compiled rbf program" << std::endl;

	IntVector subdomain_ptr(env);
	subdomain_ptr.entries.resize(n_elements.size() + 1);
	subdomain_ptr.entries[0] = 0;

	for(SizeType i = 0; i < n_elements.size(); ++i) {
		subdomain_ptr.entries[i+1] = subdomain_ptr.entries[i] + n_elements.entries[i];
	}

	subdomain_ptr.init_buffer();
	subdomain_ptr.synch_write_buffer();

	const SizeType n_weights = subdomain_ptr.entries[n_elements.size()];

	IntVector subdomain_index(env, n_weights);
	RealVector weights(env, n_weights);
	RealVector opt_epsilons(env, n_cells);

	algo_time.tick();

	if(use_dynamic_mem) {
		n_global = 1;

		if(num_threads > 0) {
			n_global = num_threads;
		} else {
			n_global = env.get_device().getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
		}

		RealVector matrix(env, n_global * max_n_dofs * max_n_dofs);

		cl::Kernel compute_rbf_weights_dynamic = cl::Kernel(rbf_prg, "compute_rbf_weights_dynamic");

		err = compute_rbf_weights_dynamic.setArg(0, rbf_type);                      assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(1, eps);                           assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(2, cell_ptr.buffer);               assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(3, cell_index.buffer);             assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(4, dims.buffer);                   assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(5, max_radius);                    assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(6, aabb.buffer);                   assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(7, rearranged_points.buffer);      assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(8, rearranged_function.buffer);    assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(9, subdomain_ptr.buffer);          assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(10, subdomain_index.buffer);       assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(11, weights.buffer);               assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(12, opt_epsilons.buffer);          assert(check_cl_error(err));
		err = compute_rbf_weights_dynamic.setArg(13, matrix.buffer);                assert(check_cl_error(err));

		err = env.get_queue().enqueueNDRangeKernel(compute_rbf_weights_dynamic, cl::NullRange, cl::NDRange(n_global), cl::NullRange); assert(check_cl_error(err));
		opt_epsilons.synch_read_buffer();
		subdomain_index.synch_read_buffer();
		weights.synch_read_buffer();
		env.get_queue().finish();

	} else {
		n_global = 1;
		if(num_threads > 0) {
			n_global = num_threads;
		} else {
			n_global = n_cells;
		}

		cl::Kernel compute_rbf_weights = cl::Kernel(rbf_prg, "compute_rbf_weights");

		err = compute_rbf_weights.setArg(0, rbf_type);                      assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(1, eps);                           assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(2, cell_ptr.buffer);               assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(3, cell_index.buffer);             assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(4, dims.buffer);                   assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(5, max_radius);                    assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(6, aabb.buffer);                   assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(7, rearranged_points.buffer);      assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(8, rearranged_function.buffer);    assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(9, subdomain_ptr.buffer);          assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(10, subdomain_index.buffer);       assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(11, weights.buffer);               assert(check_cl_error(err));
		err = compute_rbf_weights.setArg(12, opt_epsilons.buffer);          assert(check_cl_error(err));

		err = env.get_queue().enqueueNDRangeKernel(compute_rbf_weights, cl::NullRange, cl::NDRange(n_global), cl::NullRange); assert(check_cl_error(err));
		opt_epsilons.synch_read_buffer();
		subdomain_index.synch_read_buffer();
		weights.synch_read_buffer();
		env.get_queue().finish();
	}

	algo_time.tock();
	total_time += algo_time.elapsed();
	if(verbose > 0)
		std::cout<< "compute rbf weights: " << algo_time << std::endl;

	data.opt_epsilons = opt_epsilons.entries;
	data.cell_ptr = cell_ptr.entries;
	data.cell_index = cell_index.entries;
	data.dims = dims.entries;
	data.max_radius = max_radius;
	data.aabb = aabb.entries;
	data.rearranged_points = rearranged_points.entries;
	data.rearranged_function = rearranged_function.entries;
	data.subdomain_ptr = subdomain_ptr.entries;
	data.subdomain_index = subdomain_index.entries;
	data.weights = weights.entries;
	data.dim = dim;
	data.n_cells = n_cells;
	data.flags = flags;

	if(verbose > 0)
	{
		std::cout << "total_time: " << total_time << std::endl;
		std::cout << "script time: " << script_time << std::endl;
	}

	return EXIT_SUCCESS;
}



// #################################################################
// #################### interpolate  #######################
int interpolate(const RBFData &data, const std::vector<Scalar> &eval_points_v, std::vector<Scalar> &result_v,
		int verbose,
		const std::string &rbf,
		bool opt,
		bool unit_cube ,
		int num_threads,
		const std::vector<Scalar> oracle
		)
{
	// create opencl context and queue
	Env env;
	env.init();
	if(verbose > 1)
		env.describe_current_setup();

    // build programs
	cl::Program rbf_prg;
	std::string rbf_prg_str;
	if(!read_string("kernels/pum_RBF.cl", rbf_prg_str))        return EXIT_FAILURE;
	if(!env.compile_program(rbf_prg_str, data.flags, rbf_prg)) return EXIT_FAILURE;

	cl_int err = CL_SUCCESS;
	const SizeType dim = data.dim;
	const SizeType n_cells = data.n_cells;

	const SizeType rbf_type = rbf_type_from_string(rbf);
	const SizeType n_eval_points = eval_points_v.size()/dim; assert(n_eval_points*dim == eval_points_v.size());

	Scalar total_time = 0;

	if(verbose > 1)
	{
		std::cout << "Evaluation points: " << n_eval_points << std::endl;
	}

	RealVector eval_points(env);
	eval_points.entries = eval_points_v;

	RealVector opt_epsilons(env, n_cells);
	opt_epsilons.entries = data.opt_epsilons;

	IntVector cell_ptr(env);
	cell_ptr.entries = data.cell_ptr;

	IntVector cell_index(env);
	cell_index.entries = data.cell_index;

	IntVector dims(env);
	dims.entries = data.dims;

	Scalar max_radius = data.max_radius;

	RealVector aabb(env);
	aabb.entries = data.aabb;

	RealVector rearranged_points(env);
	rearranged_points.entries = data.rearranged_points;
	RealVector rearranged_function(env);
	rearranged_function.entries = data.rearranged_function;

	IntVector subdomain_ptr(env);
	subdomain_ptr.entries = data.subdomain_ptr;

	IntVector subdomain_index(env);
	subdomain_index.entries = data.subdomain_index;

	RealVector weights(env);
	weights.entries = data.weights;


	if(!eval_points.init_buffer()         || !eval_points.synch_write_buffer())  return EXIT_FAILURE;
	if(!cell_ptr.init_buffer()            || !cell_ptr.synch_write_buffer())  return EXIT_FAILURE;
	if(!cell_index.init_buffer()          || !cell_index.synch_write_buffer())  return EXIT_FAILURE;
	if(!dims.init_buffer()                || !dims.synch_write_buffer())  return EXIT_FAILURE;
	if(!aabb.init_buffer()                || !aabb.synch_write_buffer())  return EXIT_FAILURE;
	if(!rearranged_points.init_buffer()   || !rearranged_points.synch_write_buffer())  return EXIT_FAILURE;
	if(!rearranged_function.init_buffer() || !rearranged_function.synch_write_buffer())  return EXIT_FAILURE;
	if(!subdomain_ptr.init_buffer()       || !subdomain_ptr.synch_write_buffer())  return EXIT_FAILURE;
	if(!subdomain_index.init_buffer()     || !subdomain_index.synch_write_buffer())  return EXIT_FAILURE;
	if(!weights.init_buffer()             || !weights.synch_write_buffer())  return EXIT_FAILURE;

	Chrono script_time, algo_time;
	script_time.tick();
	algo_time.tick();

	SizeType n_global = n_eval_points;
	if (num_threads > 0)
	{
		n_global = num_threads;
	}

    // #init 1D output function
	RealVector result(env, n_eval_points);

	cl::Kernel rbf_interpolate = cl::Kernel(rbf_prg, "rbf_interpolate");

	err = rbf_interpolate.setArg(0, rbf_type);                      assert(check_cl_error(err));
	err = rbf_interpolate.setArg(1, opt_epsilons.buffer);           assert(check_cl_error(err));
	err = rbf_interpolate.setArg(2, cell_ptr.buffer);               assert(check_cl_error(err));
	err = rbf_interpolate.setArg(3, cell_index.buffer);             assert(check_cl_error(err));
	err = rbf_interpolate.setArg(4, dims.buffer);                   assert(check_cl_error(err));
	err = rbf_interpolate.setArg(5, max_radius);                    assert(check_cl_error(err));
	err = rbf_interpolate.setArg(6, aabb.buffer);                   assert(check_cl_error(err));
	err = rbf_interpolate.setArg(7, rearranged_points.buffer);      assert(check_cl_error(err));
	err = rbf_interpolate.setArg(8, rearranged_function.buffer);    assert(check_cl_error(err));
	err = rbf_interpolate.setArg(9, subdomain_ptr.buffer);          assert(check_cl_error(err));
	err = rbf_interpolate.setArg(10, subdomain_index.buffer);       assert(check_cl_error(err));
	err = rbf_interpolate.setArg(11, weights.buffer);               assert(check_cl_error(err));
	err = rbf_interpolate.setArg(12, n_eval_points);                assert(check_cl_error(err));
	err = rbf_interpolate.setArg(13, eval_points.buffer);           assert(check_cl_error(err));
	err = rbf_interpolate.setArg(14, result.buffer);                assert(check_cl_error(err));

	err = env.get_queue().enqueueNDRangeKernel(rbf_interpolate, cl::NullRange, cl::NDRange(n_global), cl::NullRange); assert(check_cl_error(err));
	result.synch_read_buffer();
	env.get_queue().finish();


	algo_time.tock();
	total_time += algo_time.elapsed();
	if(verbose > 0)
		std::cout<<"interpolate: " << algo_time <<std::endl;

	script_time.tock();



	if(!oracle.empty())
	{
		assert(oracle.size()==result.size());
		Scalar rmse_err = 0;
		Scalar max_err = -1;

		for (SizeType i = 0; i < oracle.size(); ++i)
		{
			const Scalar delta = oracle[i]-result.entries[i];

			rmse_err += delta*delta;
			max_err = std::max(max_err, Scalar(fabs(delta)));
		}

		rmse_err = sqrt(rmse_err/oracle.size());


		std::cout << "rmse_err: " << rmse_err << std::endl;
		std::cout << "max_err: " << max_err << std::endl;
	}

	if(verbose > 0)
	{
		std::cout << "total_time: " << total_time << std::endl;
		std::cout << "script time: " << script_time << std::endl;
	}

	result_v = result.entries;

	return EXIT_SUCCESS;
}

}

#ifndef RBF_PUM_LIB
//DEBUG: g++ -g rbf_interpolate.cpp -framework OpenCL -D_GLIBCXX_DEBUG
int main(const int argc, const char * argv[]) {
	using namespace rbf_pum;

	if(argc < 5) {
		error_usage();
		print_usage();
		return EXIT_FAILURE;
	}

	std::vector<Scalar> points, function, eval_points, result, oracle;

	if(!read_vec(argv[1], '\t', points)) return EXIT_FAILURE;
	if(!read_vec(argv[2], '\t', function)) return EXIT_FAILURE;
	if(!read_vec(argv[3], '\t', eval_points)) return EXIT_FAILURE;

	const std::string output_path = argv[4];


	std::cout << "----------------------------------\n";
	std::cout << "args:\n";
	std::cout << "\tpoints: " << argv[1] << std::endl;
	std::cout << "\tfunction: " << argv[2] << std::endl;
	std::cout << "\teval_points: " << argv[3] << std::endl;

	std::string rbf = "M4";
	int num_threads = -1;
	bool unit_cube  = false;
	std::string oraclePath = "";
	bool opt = true;


	if(argc > 6) {
		std::cout << "options: " << std::endl;
		for(int a = 5; a < argc; a += 2) {
			const std::string arg = argv[a];
			if(arg == "-r") {
				rbf = argv[a + 1];
				std::cout << "\t-r " << argv[a + 1] << std::endl;
			} else if(arg == "-p") {
				num_threads = atoi(argv[a + 1]);
				std::cout << "\t-p " << argv[a + 1] << std::endl;
			} else if(arg == "-u") {
				unit_cube = atoi(argv[a + 1]);
				std::cout << "\t-u " << argv[a + 1] << std::endl;
			} else if(arg == "-o") {
				oraclePath = argv[a + 1];
				std::cout << "\t-o " << argv[a + 1] << std::endl;
			} else if(arg == "-t") {
				opt = atoi(argv[a + 1]);
				std::cout << "\t-t " << argv[a + 1] << std::endl;
			}
		}

		std::cout << "----------------------------------\n";
	}

	if(!oraclePath.empty())
	{
		if(!read_vec(oraclePath, '\t', oracle))
		{
			std::cerr<<"Unable to read oracle at " << oraclePath <<std::endl;
		}
	}

	RBFData data;
	int outcome1 = rbf_pum::init(points, function, data, 2, rbf, opt, unit_cube, num_threads);
	if(outcome1 != EXIT_SUCCESS)
		return outcome1;

	int outcome2 = rbf_pum::interpolate(data, eval_points, result, 2, rbf, opt, unit_cube, num_threads, oracle);
	if(outcome2 != EXIT_SUCCESS)
		return outcome2;



	std::ofstream outFile (output_path.c_str());
	if (outFile.is_open())
	{
		print_vec(result, outFile);
		outFile.close();
	}
	else
	{
		std::cerr << "Unable write result to "<<output_path<<std::endl;
	}

	return outcome2;
}
#endif