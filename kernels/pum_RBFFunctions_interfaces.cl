#ifndef PUM_RBF_FUNCTIONS_INTERFACES_CL
#define PUM_RBF_FUNCTIONS_INTERFACES_CL 

#include "pum_Functions_interfaces.cl"

#ifndef MAX_N_DOFS
#error "You must define the system size with -DMAX_N_DOFS=<your_size>"
#endif //MAX_N_DOFS



#ifndef must_find_optimal_epsilon
#define must_find_optimal_epsilon 0
#endif

#ifndef N_EPSILONS
#define N_EPSILONS 50
#endif //N_EPSILONS

#ifndef CLIPP_HOST_CL	


Scalar compute_loocv_error(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	const Scalar epsilon,
	Scalar *temp_p_weights
	);

Scalar find_optimal_epsilon_brent(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	const SizeType n_epsilons,
	const Scalar *epsilons,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	Scalar *p_weights);


Scalar find_optimal_epsilon_goldsec_ab(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	const Scalar a0,
	const Scalar b0,
	Scalar *p_weights
	);

Scalar find_optimal_epsilon_goldsec(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	const SizeType n_epsilons,
	const Scalar *epsilons,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	Scalar *p_weights
	);

Scalar find_optimal_epsilon_parab(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	const SizeType n_epsilons,
	const Scalar *epsilons,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	Scalar *p_weights
	);

Scalar find_optimal_epsilon(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	const SizeType n_epsilons,
	const Scalar *epsilons,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	Scalar *p_weights);


void compute_rbf_weights_aux( const int rbf_type,								//0
							   	   const Scalar epsilon, 						//1
								   const __global SizeType *cell_ptr, 			//2
								   const __global SizeType *cell_index,			//3
								   const __global SizeType *dims, 				//4
								   const Scalar max_radius,						//5
								   const __global Scalar *aabb,					//6
								   const __global Scalar *points, 				//7
								   const __global Scalar *function,				//8
								   const __global SizeType *subdomain_ptr,		//9
								   __global SizeType *subdomain_index,			//10
								   __global Scalar *weights, 					//11
								   __global Scalar *opt_epsilons,				//12
								   MATRIX_COND_GLOBAL Scalar *global_matrix		//13 (it can be null for static sizes)
								   );	

void assemble_rbf_system(const int rbf_type,	
	const SizeType n_selected, const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	const Scalar epsilon,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs
	);


SizeType point_selection(const __global SizeType * cell_ptr, 
	const __global SizeType * cell_index, 
	const __global Scalar  * points,
	const Scalar max_radius_squared,
	const SizeType * p_dims,
	const SizeType total_dim,
	const SizeType * three_dims, 
	const SizeType *tensor_index,  
	const Scalar *center,
	SizeType *selected);

SizeType point_selection_from_subdomain_index(
	const SizeType cell,
	const __global SizeType * subdomain_ptr, 
	const __global SizeType * subdomain_index, 
	SizeType *selected);


#endif //CLIPP_HOST_CL

#endif //PUM_RBF_FUNCTIONS_INTERFACES_CL
