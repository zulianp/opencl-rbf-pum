#ifndef QR_SOLVE_INPLACE_CL
#define QR_SOLVE_INPLACE_CL

#include "QRSolveInplace_interface.cl"

//A in n x n
void qr_solve_inplace(const SizeType n, MATRIX_COND_GLOBAL Scalar *A, const Scalar *b, Scalar *x)
{

    Scalar v[MAX_N_DOFS];
    Scalar vTA[MAX_N_DOFS];


    for(SizeType i = 0; i < n; ++i)
    {
        x[i] = b[i];
    }

    for(SizeType k=0; k<n-1;++k)
    {
        Scalar g=0;
        for(SizeType i=k; i<n; ++i){
            v[i]=value_at(n,i,k,A);
            g+=v[i]*v[i];
        }

        const Scalar sqrtg=sqrt(g);

        Scalar s=2*g+2*v[k]*sqrtg;
        

        //Orthogonal transformation matrix that eliminates one element
        //below the diagonal of the matrix it is post-multiplying:

        if(fabs(s)>DEFAULT_TOLLERANCE)
        {
            v[k]+=sqrtg;

            for(SizeType j=k;j<n;++j)
                vTA[j]=0;

            s=rsqrt(s);
            Scalar xDotV=0;

            for(SizeType i=k;i<n;++i){
                v[i]*=s;
                xDotV+=x[i]*v[i];

                
                for(SizeType j=k;j<n;++j){
                    vTA[j]+=v[i]*value_at(n,i,j,A); //v'*A
                }
            }

            for(SizeType i=k;i<n;++i)
            {
                for(SizeType j=k;j<n;++j){
                    value_at(n,i,j,A)-= 2 * v[i] * vTA[j]; //A=A-2 * v * vTA; //Product HR
                }

                x[i] -= 2 * v[i] * xDotV;
            }   
        }
    }

    
    for(SizeType i=n-1; i>=0; --i){ //x=A\x;

        if(fabs(value_at(n,i,i,A))<1e-8)
        {
            x[i]=0;
        }
        else
        {
            for(SizeType j=i+1;j<n;++j)
                x[i]-=x[j]*value_at(n,i,j,A);
            
            x[i]/=value_at(n,i,i,A);
        }
    }    

}

#endif //QR_SOLVE_INPLACE_CL
