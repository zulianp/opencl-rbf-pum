

#ifndef LDL_SOLVE_IN_PLACE_CL
#define LDL_SOLVE_IN_PLACE_CL

#include "pum_KernelBase.cl"
#include "ldl_solve_in_place_interface.cl"
#include "pum_Functions_interfaces.cl"

void ldl_diagonal_of_inverse(const SizeType n, const MATRIX_COND_GLOBAL Scalar *ldl_L_and_inv_L, const Scalar *ldl_diag, Scalar *diag_of_inverse)
{
	for(SizeType i = 0; i < n; ++i) {
		diag_of_inverse[i] = 0;
	}

	for(SizeType k = 0; k < n; ++k) {
		for(SizeType i = 0; i <= k; ++i) {
			const SizeType j = i;
			diag_of_inverse[i] += value_at(n, i, k, ldl_L_and_inv_L) * 1.0/ldl_diag[k] * value_at(n, j, k, ldl_L_and_inv_L);
		}
	}
}

void ldl_decomposition_in_place(const SizeType n, MATRIX_COND_GLOBAL Scalar *A, Scalar *d)
{
	Scalar v 	[MAX_N_DOFS];
	Scalar work	[MAX_N_DOFS];

	v[0] = A[0];
	d[0] = A[0];
	A[0] = 0;

	for(SizeType i = 1; i < n; ++i) {
		value_at(n, i, 0, A) /= v[0];
	}

	const SizeType nm1 = n - 1;
	for(SizeType j = 1; j < nm1; ++j) {
		for(SizeType k = 0; k < j; ++k) {
			v[k] = value_at(n, j, k, A) * d[k];
		}

		v[j] = value_at(n, j, j, A);
		for(SizeType k = 0; k < j; ++k) {
			v[j] -=  value_at(n, j, k, A) * v[k];
		}

		d[j] = v[j];

		for(SizeType k = j+1; k < n; ++k) {
			work[k] = 0;
			for(SizeType l = 0; l < j; ++l) {
				work[k] += value_at(n, k, l, A) * v[l];
			}
		}

		for(SizeType k = j+1; k < n; ++k) {
			value_at(n, k, j, A) = (value_at(n, k, j, A) - work[k])/v[j]; 
		}
	}

	for(SizeType k = 0; k < nm1; ++k) {
		v[k] = value_at(n, nm1, k, A) * d[k];
	}

	v[nm1] = value_at(n, nm1, nm1, A);
	for(SizeType k = 0; k < nm1; ++k) {
		v[nm1] -=  value_at(n, nm1, k, A) * v[k];
	}

	d[nm1] = v[nm1];

	for(SizeType i = 0; i < n; ++i) {
		value_at(n, i, i, A) = 1;
	}
}

void ldl_solve(const SizeType n, MATRIX_COND_GLOBAL Scalar *ldl_L,  const Scalar *ldl_diag, const Scalar *b, Scalar *x)
{
	forward_substitute(n, ldl_L, b, x);
	
	for(SizeType i = 0; i < n; ++i) {
		if(fabs(ldl_diag[i]) > DEFAULT_TOLLERANCE) {
			x[i] /= ldl_diag[i];
		}
	}

	backward_substitute_transpose_in_place(n, ldl_L, x);
}

//A in n x n
void ldl_solve_in_place(const SizeType n, MATRIX_COND_GLOBAL Scalar *A, const Scalar *b, Scalar *x)
{
	Scalar d 	[MAX_N_DOFS];
	ldl_decomposition_in_place(n, A, d);
	ldl_solve(n, A, d, b, x);
}

void ldl_inverse_transposed_of_lower_tri(const SizeType n, const MATRIX_COND_GLOBAL Scalar *ldl_L, MATRIX_COND_GLOBAL Scalar * ldl_inv_L)
{
	for(SizeType col = 0; col < n; ++col) {
		for(SizeType i = col + 1; i < n; ++i) {

			value_at(n, col, i, ldl_inv_L) = 0;

			for(SizeType j = col; j < i; ++j) {
				value_at(n, col, i, ldl_inv_L) -= value_at(n, i, j, ldl_L) * value_at(n, col, j, ldl_inv_L);
			}

			value_at(n, col, i, ldl_inv_L) /= value_at(n, i, i, ldl_L);
		}
	}
}

#endif //LDL_SOLVE_IN_PLACE_CL
