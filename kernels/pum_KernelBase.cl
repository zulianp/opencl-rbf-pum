#ifndef KERNEL_BASE_CL
#define KERNEL_BASE_CL

typedef int SizeType;

#ifdef USE_SINGLE_PRECISION
typedef float Scalar;
#define LARGE_VALUE 1e7
#define DEFAULT_TOLLERANCE 1e-7
#else
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
typedef double Scalar;
#define LARGE_VALUE 1e14
#define DEFAULT_TOLLERANCE 1e-15
#endif //USE_SINGLE_PRECISION


#ifndef N_DIMS
#error "N_DIMS must be specified! Use -DN_DIMS=<your_dims>"
#endif

#define generic_copy(n, src, dest) { for(SizeType macro_index = 0; macro_index < (n); ++macro_index) { (dest)[macro_index] = (src)[macro_index]; } }
#define generic_set(n, value, values) { for(SizeType macro_index = 0; macro_index < (n); ++macro_index) { (values)[macro_index] = (value); } }
#define generic_ptr_swap(T, left, right) { T * temp = left; left = right; right = temp; }
#define generic_swap(T, left, right) { T temp = left; left = right; right = temp; }
#define loop_k(n, expr_with_array_k) { for(SizeType k = 0; k < (n); ++k) {  (expr_with_array_k); } }

#define INVALID_INDEX -1

//Set DEBUG_KERNEL for debugging
// #define DEBUG_KERNEL
#ifdef DEBUG_KERNEL
	#define REPORT_OUT_OF_RANGE_ERROR(msg, value, lower, upper) printf("[Error] %s %d not in range (%d, %d)\n", msg, value, lower, upper)
	#define REPORT_ERROR(x) printf("[Error] %s", x)
	#define ASSERT(x, msg) if(!(x)) printf("[Error] %s\n", (msg)) 
	#define FEOBJECT_DEBUG(fe_ptr) init_invalid(fe_ptr)
#else
#define REPORT_OUT_OF_RANGE_ERROR(msg, value, lower, upper)
	#define REPORT_ERROR(x)
	#define ASSERT(x, msg) 
	#define FEOBJECT_DEBUG(fe_ptr)
#endif //DEBUG_KERNEL


#define min_n(left, right, result)								\
{																\
	for(SizeType i = 0; i < N_DIMS; ++i) {						\
		(result)[i] = min((left)[i], (right)[i]);				\
	}															\
}

#define max_n(left, right, result)								\
{																\
	for(SizeType i = 0; i < N_DIMS; ++i) {						\
		(result)[i] = max((left)[i], (right)[i]);				\
	}															\
}

#define min_max_n(left, right, result)									\
{																		\
	min_n(left, right, result);											\
	max_n((&left[N_DIMS]), (&right[N_DIMS]), (&result[N_DIMS]));		\
}

#endif //KERNEL_BASE_CL

