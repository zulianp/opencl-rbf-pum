#ifndef PUM_FUNCTION_INTERFACES
#define PUM_FUNCTION_INTERFACES

#include "pum_KernelBase.cl"

#define value_at(n, i, j, A) (A[(i) * ((n)) + (j)])  

#ifdef USE_GLOBAL_MATRIX
#define MATRIX_COND_GLOBAL __global
#else
#define MATRIX_COND_GLOBAL
#endif


#ifndef CLIPP_HOST_CL	

bool is_nan(const Scalar number);
Scalar distance_d(const Scalar *left, const Scalar *right);
Scalar distance_squared_d(const Scalar *left, const Scalar *right);

///////////////////////////////////////////////////////////////////////////////////////////////////////////

void matrix_print(const SizeType rows, const SizeType cols, const Scalar *matrix);
void matrix_swap_rows(const SizeType columns, const SizeType row_1, const SizeType row_2, Scalar *matrix);
void matrix_swap_rows_global(const SizeType columns, const SizeType row_1, const SizeType row_2, __global Scalar *matrix);

void forward_substitute(const SizeType n, MATRIX_COND_GLOBAL Scalar *A, const Scalar *b, Scalar *x);
void backward_substitute_transpose_in_place(const SizeType n, MATRIX_COND_GLOBAL Scalar *A, Scalar *x);

/////////////////////////////////////////////////////////////////////////////////////////////////////////

void reduce_aabbs(const Scalar *private_aabb, __local Scalar *local_work, __global Scalar *result);
SizeType hash(const Scalar *point, const Scalar *aabb_min, const Scalar *aabb_range, const SizeType *dims);
void linear_index_to_tensor_index(const SizeType *dims, const SizeType index, SizeType *tensor_index);
SizeType tensor_index_to_linear_index(const SizeType *dims, const SizeType *tensor_index);
bool is_boundary(const SizeType *tensor_index, const SizeType * dims);
bool is_outside(const SizeType *tensor_index, const SizeType * dims);
SizeType shifted_index_from_tensor_index(const SizeType *three_dims, const SizeType *dims, const SizeType j, const SizeType *tensor_index);

void cell_center(const SizeType *tensor_index, const SizeType * dims, const Scalar * aabb_min, const Scalar * aabb_range, Scalar * center);
void shifted_tensor_index(const SizeType *three_dims, const SizeType *dims, const SizeType j, const SizeType *tensor_index, SizeType * result);

#endif //CLIPP_HOST_CL

#endif //PUM_FUNCTION_INTERFACES
