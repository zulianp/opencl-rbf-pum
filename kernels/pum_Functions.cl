#ifndef PUM_FUNCTION
#define PUM_FUNCTION

#include "pum_Functions_interfaces.cl"
#include "pum_RBFKernels.cl"
#include "pum_Reduce.cl"

bool is_nan(const Scalar number)
{
	return !(number == number);
}

Scalar distance_d(const Scalar *left, const Scalar *right)
{
	return sqrt(distance_squared_d(left, right));
}

Scalar distance_squared_d(const Scalar *left, const Scalar *right)
{
	Scalar result = left[0] - right[0];
	result *= result;

	for(SizeType i = 1; i < N_DIMS; ++i) {
		const Scalar temp = left[i] - right[i];
		result += temp * temp;
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
void matrix_print(const SizeType rows, const SizeType cols, const Scalar *matrix) {
	printf("[\n"); //]
	for(SizeType i = 0; i < rows; ++i) {
		for(SizeType j = 0; j < cols; ++j) {
			printf("\t%g", matrix[i*cols+j]);
		}
		printf("\n");
	}

	//[
	printf("]\n");
}

void matrix_swap_rows(const SizeType columns, const SizeType row_1, const SizeType row_2, Scalar *matrix)
{
    const SizeType offset_1 = row_1 * columns;
    const SizeType offset_2 = row_2 * columns;

    for(SizeType j = 0; j < columns; ++j) {
        const Scalar temp    = matrix[offset_1 + j];
        matrix[offset_1 + j] = matrix[offset_2 + j];
        matrix[offset_2 + j] = temp;
    }
}

void matrix_swap_rows_global(const SizeType columns, const SizeType row_1, const SizeType row_2, __global Scalar *matrix)
{
    const SizeType offset_1 = row_1 * columns;
    const SizeType offset_2 = row_2 * columns;

    for(SizeType j = 0; j < columns; ++j) {
        const Scalar temp    = matrix[offset_1 + j];
        matrix[offset_1 + j] = matrix[offset_2 + j];
        matrix[offset_2 + j] = temp;
    }
}

void forward_substitute(const SizeType n, MATRIX_COND_GLOBAL Scalar *A, const Scalar *b, Scalar *x)
{
	for(SizeType i = 0; i < n; ++i) {
		if(fabs(value_at(n,i,i,A))<1e-8) {
			x[i] = 0;
		} else {
			x[i] = b[i];
			for(SizeType j = 0; j < i; ++j) {
				x[i] -= value_at(n, i, j, A) * x[j];
			}

			x[i] /= value_at(n, i, i, A);
		}
	}
}

void backward_substitute_transpose_in_place(const SizeType n, MATRIX_COND_GLOBAL Scalar *A, Scalar *x)
{
	for(SizeType i = n-1; i >= 0; --i) {
		if(fabs(value_at(n,i,i,A))<1e-8) {
			x[i] = 0;
		} else {
			for(SizeType j = i+1; j < n; ++j) {
				x[i] -= value_at(n, j, i, A) * x[j];
			}

			x[i] /= value_at(n, i, i, A);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void reduce_aabbs(const Scalar *private_aabb, __local Scalar *local_work, __global Scalar *result)
{
	generic_reduce_private_values(1, (N_DIMS*2), private_aabb, local_work, result, min_max_n);
}


bool is_outside(const SizeType *tensor_index, const SizeType * dims)
{
	for(SizeType i = 0; i < N_DIMS; ++i) {
		if(tensor_index[i] < 0 || tensor_index[i] > dims[i]-1) return true;
	}

	return false;
}



bool is_boundary(const SizeType *tensor_index, const SizeType * dims)
{
	for(SizeType i = 0; i < N_DIMS; ++i) {
		if(tensor_index[i] == 0 || tensor_index[i] == dims[i]-1) return true;
	}

	return false;
}

SizeType shifted_index_from_tensor_index(const SizeType *three_dims, const SizeType *dims, const SizeType j, const SizeType *tensor_index)
{
	SizeType shifted_tensor_index[N_DIMS];
	linear_index_to_tensor_index(three_dims, j, shifted_tensor_index);

	loop_k(N_DIMS, shifted_tensor_index[k] += tensor_index[k] - 1 );
	return tensor_index_to_linear_index(dims, shifted_tensor_index);
}


SizeType tensor_index_to_linear_index(const SizeType *dims, const SizeType *tensor_index)
{
	SizeType result = tensor_index[0];

	for(SizeType i = 1; i < N_DIMS; ++i){
		result *= dims[i];
		result += tensor_index[i];
	}

	return result;
}

void linear_index_to_tensor_index(const SizeType *dims, const SizeType index, SizeType *tensor_index)
{
	SizeType current = index;
	const SizeType last = N_DIMS - 1;

	for(SizeType i = last; i >= 0; --i) {
		// tensor_index[i] = current % dims[i];
		// current /= dims[i];

		const SizeType next = current / dims[i];
		tensor_index[i] = current - next * dims[i];
		current = next;
	}

	ASSERT(index == tensor_index_to_linear_index(dims, tensor_index), "index != tensor_index_to_linear_index(dims, tensor_index)");
}


SizeType hash(const Scalar *point, const Scalar *aabb_min, const Scalar *aabb_range, const SizeType *dims)
{
	Scalar x = (point[0] - aabb_min[0])/aabb_range[0];
	SizeType result = floor(x * dims[0]);

	SizeType totalDim = dims[0];

	for(SizeType i = 1; i < N_DIMS; ++i) {
		result *= dims[i];

		x = (point[i] - aabb_min[i])/aabb_range[i];
		result += floor(x * dims[i]);
		totalDim *= dims[i];
	}

	if(result >= totalDim || result < 0) {
		result = -1;
		// printf("error -> %d\n", result);
	}

	return result;
}

void cell_center(const SizeType *tensor_index, const SizeType * dims, const Scalar * aabb_min, const Scalar * aabb_range, Scalar * center)
{
	loop_k(N_DIMS, center[k] = aabb_min[k] + (tensor_index[k] + 0.5) * aabb_range[k]/dims[k]);
}

void shifted_tensor_index(const SizeType *three_dims, const SizeType *dims, const SizeType j, const SizeType *tensor_index, SizeType * result)
{
	linear_index_to_tensor_index(three_dims, j, result);
	loop_k(N_DIMS, result[k] += tensor_index[k] - 1 );
}

#endif //PUM_FUNCTION
