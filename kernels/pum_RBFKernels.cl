#ifndef PUM_RBF_KENRELS_CL
#define PUM_RBF_KENRELS_CL 

#include "pum_KernelBase.cl"

#ifndef CLIPP_HOST_CL	

Scalar rbf_kernel_function(const int type, const Scalar eps, const Scalar radius);

Scalar rbf_gaussian(const Scalar epsilon, const Scalar radius);

Scalar rbf_inverse_multi_quadric(const Scalar epsilon, const Scalar radius);

Scalar rbf_matern_2(const Scalar epsilon, const Scalar radius);
Scalar rbf_matern_4(const Scalar epsilon, const Scalar radius);
Scalar rbf_matern_6(const Scalar epsilon, const Scalar radius);

Scalar rbf_wendland_2(const Scalar epsilon, const Scalar radius);
Scalar rbf_wendland_4(const Scalar epsilon, const Scalar radius);
Scalar rbf_wendland_6(const Scalar epsilon, const Scalar radius);

#endif //CLIPP_HOST_CL




enum RBFType {
	GA = 0,
	IMQ,
	M2,
	M4,
	M6,
	W2,
	W4,
	W6,
	INVALID
};

//tag: GA  smoothness: (C^\infty)
Scalar rbf_gaussian(const Scalar epsilon, const Scalar radius)
{
	const Scalar eps_2 = epsilon * epsilon;
	const Scalar radius_2 = radius * radius;
	return exp(-eps_2 * radius_2);
}

//tag: IMQ  smoothness: (C^\infty)
Scalar rbf_inverse_multi_quadric(const Scalar epsilon, const Scalar radius)
{
	const Scalar eps_2 = epsilon * epsilon;
	const Scalar radius_2 = radius * radius;
	return rsqrt(1.0 + eps_2 * radius_2);
}

Scalar rbf_matern_2(const Scalar epsilon, const Scalar radius)
{
	const Scalar eps_r = epsilon * radius;
	return exp(-eps_r) * (eps_r + 1.0);
}

Scalar rbf_matern_4(const Scalar epsilon, const Scalar radius)
{
	const Scalar eps_r = epsilon * radius;
	return exp(-eps_r) * (eps_r * eps_r + 3.0 * eps_r + 3.0);
}

//tag: M6  smoothness: (C^6)
Scalar rbf_matern_6(const Scalar epsilon, const Scalar radius)
{
	const Scalar eps_2 = epsilon * epsilon;
	const Scalar eps_3 = eps_2 * epsilon;

	const Scalar radius_2 = radius * radius;
	const Scalar radius_3 = radius_2 * radius;

	return exp(-epsilon * radius) * (eps_3 * radius_3 + 6.0 * eps_2 * radius_2 + 15.0 * epsilon * radius + 15.0) ;
}


//tag: W2  smoothness: (C^2)
Scalar rbf_wendland_2(const Scalar epsilon, const Scalar radius)
{
	const Scalar rte = epsilon * radius;
	Scalar term = 1.0 - rte;
	term *= term;
	term *= term;
	const Scalar zero=0;
	return max(zero, term) * (4.0 * rte + 1.0);
}


Scalar rbf_wendland_4(const Scalar epsilon, const Scalar radius)
{
	const Scalar eps_r = epsilon * radius;
	Scalar term = 1.0 - eps_r;
	term *= term;
	const Scalar temp = term;
	term *= term;
	term *= temp;
	
	const Scalar zero=0;
	return max(zero, term) * (35.0 * eps_r * eps_r + 18.0 * eps_r + 3.0);
}

Scalar rbf_wendland_6(const Scalar epsilon, const Scalar radius)
{
	const Scalar eps_r = epsilon * radius;
	Scalar term = 1.0 - eps_r;
	term *= term;
	term *= term;
	term *= term;

	const Scalar eps_r_2 = eps_r * eps_r;
	
	const Scalar zero=0;
	return max(zero, term) * (32.0 * eps_r * eps_r_2 + 25.0 * eps_r_2 + 8.0 * eps_r + 1.0);
}


Scalar rbf_kernel_function(const int type, const Scalar eps, const Scalar radius) 
{
	switch(type) {
		case GA:  return rbf_gaussian(eps, radius); 
		
		case IMQ: return rbf_inverse_multi_quadric(eps, radius); 

		case M2:  return rbf_matern_2(eps, radius); 
		case M4:  return rbf_matern_4(eps, radius); 
		case M6:  return rbf_matern_6(eps, radius); 

		case W2:  return rbf_wendland_2(eps, radius); 
		case W4:  return rbf_wendland_4(eps, radius); 
		case W6:  return rbf_wendland_6(eps, radius); 

		default: { 
			ASSERT(false, "rbf_kernel_function: INVALID, using gaussian");
			return rbf_gaussian(eps, radius); 
		}
	}
}


#define rbf_shepard_function(eps, radius) rbf_wendland_2(eps, radius)


#endif //PUM_RBF_KENRELS_CL
