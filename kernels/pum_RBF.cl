
#include "pum_KernelBase.cl"
#include "pum_Functions.cl"
#include "pum_RBFFunctions.cl"

#include "ldl_solve_in_place.cl"

__kernel void compute_rbf_weights( const int rbf_type,							//0
							   	   const Scalar epsilon, 						//1
								   const __global SizeType *cell_ptr, 			//2
								   const __global SizeType *cell_index,			//3
								   const __global SizeType *dims, 				//4
								   const Scalar max_radius,						//5
								   const __global Scalar *aabb,					//6
								   const __global Scalar *points, 				//7
								   const __global Scalar *function,				//8
								   const __global SizeType *subdomain_ptr,		//9
								   __global SizeType *subdomain_index,			//10
								   __global Scalar *weights, 					//11
								   __global Scalar *opt_epsilons				//12
								   )
{

#ifdef USE_GLOBAL_MATRIX
	printf("[Error] USE_GLOBAL_MATRIX defined use compute_rbf_weights_dynamic\n");
#else
	Scalar matrix[MAX_N_DOFS * MAX_N_DOFS];
	compute_rbf_weights_aux(rbf_type, epsilon, cell_ptr, cell_index, dims,
		max_radius, aabb, points, function, subdomain_ptr, subdomain_index, weights, opt_epsilons, matrix);
 #endif
}

__kernel void compute_rbf_weights_dynamic(
	const int rbf_type,
	const Scalar epsilon,
	const __global SizeType *cell_ptr,
	const __global SizeType *cell_index,
	const __global SizeType *dims,
	const Scalar max_radius,
	const __global Scalar *aabb,
	const __global Scalar *points,
	const __global Scalar *function,
	const __global SizeType *subdomain_ptr,
	__global SizeType *subdomain_index,
	__global Scalar *weights,
	__global Scalar *opt_epsilons,
	__global Scalar *global_matrix
	)
{

#ifdef USE_GLOBAL_MATRIX
	compute_rbf_weights_aux(rbf_type, epsilon, cell_ptr, cell_index, dims,
		max_radius, aabb, points, function, subdomain_ptr, subdomain_index, weights, opt_epsilons, &global_matrix[get_global_id(0) * MAX_N_DOFS * MAX_N_DOFS]);
#else
	printf("[Error] USE_GLOBAL_MATRIX not defined use compute_rbf_weights\n");
 #endif
}

__kernel void rbf_interpolate( const int rbf_type,							//0
							    const __global Scalar *epsilons,			//1
							   const __global SizeType *cell_ptr, 			//2
							   const __global SizeType *cell_index,			//3
							   const __global SizeType *dims, 				//4
							   const Scalar max_radius,						//5
							   const __global Scalar *aabb,					//6
							   const __global Scalar *points, 				//7
							   const __global Scalar *function,				//8
							   const __global SizeType *subdomain_ptr,		//9
							   const __global SizeType *subdomain_index,	//10
						       const __global Scalar *weights,			    //11
							   const SizeType n_eval_points,				//12
							   const __global Scalar * eval_points,			//13
							   __global Scalar *result 						//14
							   )
{
	Scalar p 	 [N_DIMS];
	Scalar q 	 [N_DIMS];
	Scalar center[N_DIMS];

	Scalar aabb_min  [N_DIMS];
	Scalar aabb_range[N_DIMS];

	SizeType tensor_index [N_DIMS];
	SizeType p_dims		  [N_DIMS];
	SizeType three_dims   [N_DIMS];
	SizeType s_t_index 	  [N_DIMS];

	SizeType selected 	  [MAX_N_DOFS];

	generic_copy(N_DIMS, dims, p_dims);
	generic_copy(N_DIMS, aabb, aabb_min);
	loop_k(N_DIMS, aabb_range[k] = aabb[N_DIMS+k] - aabb_min[k]);

	SizeType total_dim = 3;
	loop_k(N_DIMS-1, total_dim *= 3);
	loop_k(N_DIMS, three_dims[k] = 3);

	const Scalar max_radius_squared = max_radius*max_radius;
	const Scalar shepard_esp = 1.0/max_radius;

	for(SizeType i = get_global_id(0); i < n_eval_points; i+= get_global_size(0)) {
		const SizeType point_offset = i * N_DIMS;
		generic_copy(N_DIMS, &eval_points[point_offset], p);

		const SizeType parent_cell_index = hash(p, aabb_min, aabb_range, p_dims);
		bool at_least_one_considered = false;

		//outside the range
		if(parent_cell_index == -1)
		{
			result[i] = 0;
			at_least_one_considered = true;
			continue;
		}

		linear_index_to_tensor_index(p_dims, parent_cell_index, tensor_index);

		Scalar interpolated_result = 0;
		Scalar total_shepard_weight = 0;

		for(SizeType j = 0; j < total_dim; ++j) {
			shifted_tensor_index(three_dims, p_dims, j, tensor_index, s_t_index);
			if(is_boundary(s_t_index, p_dims) || is_outside(s_t_index, p_dims)) continue;

			cell_center(s_t_index, p_dims, aabb_min, aabb_range, center);

			const Scalar dist_from_center = distance_squared_d(p, center);
			if(dist_from_center >= max_radius_squared) continue;

			const SizeType shifted_index = tensor_index_to_linear_index(p_dims, s_t_index);
			Scalar rbf_result_j = 0;

			const SizeType n_selected = point_selection_from_subdomain_index(shifted_index, subdomain_ptr, subdomain_index, selected);

			at_least_one_considered = true;

			if(n_selected <= 0) continue;

			const Scalar epsilon = epsilons[shifted_index];

			for(SizeType l = 0; l < n_selected; ++l) {

				const SizeType point_index = selected[l];
				const SizeType point_offset = point_index * N_DIMS;

				generic_copy(N_DIMS, &points[point_offset], q);

				const Scalar dist   = distance_d(p, q);
				const Scalar weight = weights[ subdomain_ptr[shifted_index] + l];


				rbf_result_j += weight * rbf_kernel_function(rbf_type, epsilon, dist);
			}


			ASSERT( (sqrt(dist_from_center) / max_radius) < 1, "rbf_interpolate: (sqrt(dist_from_center) / max_radius) < 1" );

			const Scalar shepard_weight = rbf_shepard_function( shepard_esp, sqrt(dist_from_center) / max_radius );

			ASSERT(shepard_weight > 0, "rbf_interpolate: shepard_weight > 0");

			interpolated_result += rbf_result_j * shepard_weight;
			total_shepard_weight += shepard_weight;
		}

		if(!at_least_one_considered) {
			result[i] = 0.0;
			// matrix_print(N_DIMS, 1, p);
			continue;
		}

		ASSERT(at_least_one_considered, "rbf_interpolate: at_least_one_considered");
		ASSERT(total_shepard_weight > 0, "rbf_interpolate: total_shepard_weight > 0");
		ASSERT(!is_nan(interpolated_result), "rbf_interpolate: !is_nan(interpolated_result)");

		interpolated_result /= total_shepard_weight;
		result[i] = interpolated_result;
	}
}
