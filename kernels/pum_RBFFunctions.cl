#ifndef PUM_RBF_FUNCTIONS_CL
#define PUM_RBF_FUNCTIONS_CL

#include "pum_RBFFunctions_interfaces.cl"
#include "pum_RBFKernels.cl"

#include "ldl_solve_in_place.cl"

#define MIN_EPS 10
#define MAX_EPS 20

SizeType point_selection(
	const __global SizeType * cell_ptr, 
	const __global SizeType * cell_index, 
	const __global Scalar  * points,
	const Scalar max_radius_squared,
	const SizeType * p_dims,
	const SizeType total_dim,
	const SizeType * three_dims, 
	const SizeType *tensor_index,  
	const Scalar *center,
	SizeType *selected)
{
	Scalar p[N_DIMS];

	SizeType n_selected = 0;
	for(SizeType j = 0; j < total_dim; ++j) {
		const SizeType shifted_index =  shifted_index_from_tensor_index(three_dims, p_dims, j, tensor_index);

		const SizeType begin = cell_ptr[shifted_index];
		const SizeType end   = cell_ptr[shifted_index + 1];


		ASSERT(begin <= end, "begin <= end");

		for(SizeType l = begin; l != end; ++l) {
			const SizeType point_index = cell_index[l];
			const SizeType point_offset = point_index * N_DIMS;

			generic_copy(N_DIMS, &points[point_offset], p);

			if(distance_squared_d(p, center) < max_radius_squared) {
				ASSERT(n_selected < MAX_N_DOFS, "point_selection: violated n_selected < MAX_N_DOFS");
				selected[n_selected++] = point_index;
			}
		}
	}

	return n_selected;
}

SizeType point_selection_from_subdomain_index(
	const SizeType cell,
	const __global SizeType * subdomain_ptr, 
	const __global SizeType * subdomain_index, 
	SizeType *selected)
{
	const SizeType n_selected = subdomain_ptr[cell+1] - subdomain_ptr[cell];
	const SizeType offset 	  = subdomain_ptr[cell];

	generic_copy(n_selected, &subdomain_index[offset], selected);

	return n_selected;
}



void assemble_rbf_system(const int rbf_type,	
	const SizeType n_selected, const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	const Scalar epsilon,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs
	)
{

	Scalar p 	 [N_DIMS];
	Scalar q 	 [N_DIMS];

	ASSERT(n_selected <= MAX_N_DOFS, "too many dofs");
	generic_set(n_selected * n_selected, 0, matrix);

	for(SizeType j = 0; j < n_selected; ++j) {
		const SizeType offset_j = j * n_selected;
		const SizeType selected_j = selected[j];

		generic_copy(N_DIMS, &points[selected_j*N_DIMS], p);

		rhs[j] = function[selected_j];

		for(SizeType l = j; l < n_selected; ++l) {
			const SizeType offset_l = l * n_selected;
			const SizeType selected_l = selected[l];

			generic_copy(N_DIMS, &points[selected_l * N_DIMS], q);

			const Scalar r = distance_d(p, q);
			matrix[offset_j + l] = rbf_kernel_function(rbf_type, epsilon, r);
			matrix[offset_l + j] = matrix[offset_j + l];
		}
	}
}

Scalar compute_loocv_error(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	const Scalar epsilon,
	Scalar *temp_p_weights
	)
{

	Scalar d 			 [MAX_N_DOFS];
	Scalar d_inv 		 [MAX_N_DOFS];

	assemble_rbf_system(rbf_type, n_selected, selected, points, function, epsilon, matrix, rhs);

	ldl_decomposition_in_place(n_selected, matrix, d);

	ldl_solve(n_selected, matrix, d, rhs, temp_p_weights);

	ldl_inverse_transposed_of_lower_tri(n_selected, matrix, matrix);
	ldl_diagonal_of_inverse(n_selected, matrix, d, d_inv);

	Scalar current_energy = 0;
	loop_k(n_selected, current_energy = max(current_energy, (fabs(d_inv[k]) < DEFAULT_TOLLERANCE)? 10000 : temp_p_weights[k]/d_inv[k]) );

	return current_energy;
}


Scalar find_optimal_epsilon_brent(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	const SizeType n_epsilons,
	const Scalar *epsilons,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	Scalar *p_weights)
{
	const Scalar t=1e-8;
	const SizeType n_iter=100;

	const Scalar a = epsilons[0];
	const Scalar b = epsilons[n_epsilons-1];

  Scalar c;
  Scalar d;
  Scalar e;
  Scalar eps;
  Scalar fu;
  Scalar fv;
  Scalar fw;
  Scalar fx;
  Scalar m;
  Scalar p;
  Scalar q;
  Scalar r;
  Scalar sa;
  Scalar sb;
  Scalar t2;
  Scalar tol;
  Scalar u;
  Scalar v;
  Scalar w;
  Scalar x;

  c = 0.5 * ( 3.0 - sqrt ( 5.0 ) );

  eps = 1e-8;

  sa = a;
  sb = b;
  x = sa + c * ( b - a );
  w = x;
  v = w;
  e = 0.0;
  fx = compute_loocv_error(
		rbf_type,
		n_selected,
		selected,
		points,
		function,
		matrix,
		rhs,
		x,
		p_weights
		);


  fw = fx;
  fv = fw;

  for (SizeType i=0;i<n_iter;++i)
  {
    m = 0.5 * ( sa + sb ) ;
    tol = eps * fabs( x ) + t;
    t2 = 2.0 * tol;
//
//  Check the stopping criterion.
//
    if ( fabs( x - m ) <= t2 - 0.5 * ( sb - sa ) )
    {
      break;
    }
//
//  Fit a parabola.
//
    r = 0.0;
    q = r;
    p = q;

    if ( tol < fabs( e ) )
    {
      r = ( x - w ) * ( fx - fv );
      q = ( x - v ) * ( fx - fw );
      p = ( x - v ) * q - ( x - w ) * r;
      q = 2.0 * ( q - r );
      if ( 0.0 < q )
      {
        p = - p;
      }
      q = fabs( q );
      r = e;
      e = d;
    }

    if ( fabs( p ) < fabs( 0.5 * q * r ) &&
         q * ( sa - x ) < p &&
         p < q * ( sb - x ) )
    {
//
//  Take the parabolic interpolation step.
//
      d = p / q;
      u = x + d;
//
//  F must not be evaluated too close to A or B.
//
      if ( ( u - sa ) < t2 || ( sb - u ) < t2 )
      {
        if ( x < m )
        {
          d = tol;
        }
        else
        {
          d = - tol;
        }
      }
    }
//
//  A golden-section step.
//
    else
    {
      if ( x < m )
      {
        e = sb - x;
      }
      else
      {
        e = sa - x;
      }
      d = c * e;
    }
//
//  F must not be evaluated too close to X.
//
    if ( tol <= fabs( d ) )
    {
      u = x + d;
    }
    else if ( 0.0 < d )
    {
      u = x + tol;
    }
    else
    {
      u = x - tol;
    }

    fu = compute_loocv_error(
		rbf_type,
		n_selected,
		selected,
		points,
		function,
		matrix,
		rhs,
		u,
		p_weights
		);
//
//  Update A, B, V, W, and X.
//
    if ( fu <= fx )
    {
      if ( u < x )
      {
        sb = x;
      }
      else
      {
        sa = x;
      }
      v = w;
      fv = fw;
      w = x;
      fw = fx;
      x = u;
      fx = fu;
    }
    else
    {
      if ( u < x )
      {
        sa = u;
      }
      else
      {
        sb = u;
      }

      if ( fu <= fw || w == x )
      {
        v = w;
        fv = fw;
        w = u;
        fw = fu;
      }
      else if ( fu <= fv || v == x || v == w )
      {
        v = u;
        fv = fu;
      }
    }
  }
  return x;
}



Scalar find_optimal_epsilon_parab(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	const SizeType n_epsilons,
	const Scalar *epsilons,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	Scalar *p_weights
	)
{
	const Scalar n_iter=100;
	Scalar a = epsilons[0];
	Scalar c = epsilons[n_epsilons-1];
	Scalar b = (a+c)/2;

	Scalar fa = compute_loocv_error(
		rbf_type,
		n_selected,
		selected,
		points,
		function,
		matrix,
		rhs,
		a,
		p_weights
		);

	Scalar fb = compute_loocv_error(
		rbf_type,
		n_selected,
		selected,
		points,
		function,
		matrix,
		rhs,
		b,
		p_weights
		);

	Scalar fc = compute_loocv_error(
		rbf_type,
		n_selected,
		selected,
		points,
		function,
		matrix,
		rhs,
		c,
		p_weights
		);


	Scalar opt_epsilon = -1;
	for(SizeType i=0;i<n_iter;++i)
	{
		const Scalar denom = (2*((fb-fc)*(b-a) - (fb-fa)*(b-c)));

		if(fabs(denom)<1e-8)
			return opt_epsilon;
		// find_optimal_epsilon_goldsec_ab(rbf_type, n_selected, selected, 
		// 		points, function, 
		// 		matrix, rhs, 
		// 		a,c,
		// 		p_weights);


		const Scalar x = b - ((fb-fc)*(b-a)*(b-a) - (fb-fa)*(b-c)*(b-c)) / denom;
		opt_epsilon = x;

		const Scalar fx = compute_loocv_error(
			rbf_type,
			n_selected,
			selected,
			points,
			function,
			matrix,
			rhs,
			x,
			p_weights
			);


		if (x > b){
			if (fx > fb){
				c = x;
				fc = fx;
			}
			else{
				a = b;
				fa = fb;
				b = x;
				fb = fx;
			}
		}
		else{
			if (fx > fb){
				a = x;
				fa = fx;
			}
			else{
				c = b;
				fc = fb;
				b = x;
				fb = fx;
			}
		}
	}

	return opt_epsilon;
}


Scalar find_optimal_epsilon_goldsec(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	const SizeType n_epsilons,
	const Scalar *epsilons,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	Scalar *p_weights
	)
{
	return find_optimal_epsilon_goldsec_ab(rbf_type, n_selected, selected, 
		points, function,
		matrix, rhs, 
		epsilons[0], epsilons[n_epsilons-1],
		p_weights);
}

Scalar find_optimal_epsilon_goldsec_ab(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	const Scalar a0,
	const Scalar b0,
	Scalar *p_weights
	)
{
	const Scalar tol=1e-5;

	const Scalar tau = (sqrt(5.0) - 1)/2;

	Scalar a = a0;
	Scalar b = b0;

	Scalar x1 = a + (1 - tau)*(b-a);
	Scalar x2 = a + tau*(b-a);

	Scalar f1 = compute_loocv_error(
		rbf_type,
		n_selected,
		selected,
		points,
		function,
		matrix,
		rhs,
		x1,
		p_weights
		);

	Scalar f2 = compute_loocv_error(
		rbf_type,
		n_selected,
		selected,
		points,
		function,
		matrix,
		rhs,
		x2,
		p_weights
		);

	while (fabs(b-a) > tol){
		if (f1 > f2){
			a = x1;

			x1 = x2;
			x2 = a + tau*(b-a);
			f1 = f2;
			f2 = compute_loocv_error(
				rbf_type,
				n_selected,
				selected,
				points,
				function,
				matrix,
				rhs,
				x2,
				p_weights
				);
		}
		else{
			b = x2;

			x2 = x1;
			x1 = a + (1 - tau)*(b-a);
			f2 = f1;
			f1 = compute_loocv_error(
				rbf_type,
				n_selected,
				selected,
				points,
				function,
				matrix,
				rhs,
				x1,
				p_weights
				);
		}
	}

	Scalar opt_epsilon = (a+b)/2;

	compute_loocv_error(
		rbf_type,
		n_selected,
		selected,
		points,
		function,
		matrix,
		rhs,
		x1,
		p_weights
		);

	return opt_epsilon;
}

Scalar find_optimal_epsilon(
	const int rbf_type,	
	const SizeType n_selected, 
	const SizeType * selected, 
	const __global Scalar *points, 
	const __global Scalar *function,
	const SizeType n_epsilons,
	const Scalar *epsilons,
	MATRIX_COND_GLOBAL Scalar *matrix,
	Scalar *rhs,
	Scalar *p_weights)
{

	Scalar temp_p_weights[MAX_N_DOFS];

	Scalar opt_energy = LARGE_VALUE;
	Scalar opt_epsilon = -1;

	for(SizeType i = 0; i < n_epsilons; ++i) {
		const Scalar epsilon = epsilons[i];

		const Scalar current_energy =compute_loocv_error(
			rbf_type,
			n_selected,
			selected,
			points,
			function,
			matrix,
			rhs,
			epsilon,
			temp_p_weights
			);

		if(current_energy < opt_energy) {
			opt_energy 	= current_energy;
			opt_epsilon = epsilon;
			generic_copy(n_selected, temp_p_weights, p_weights);
		}
	}

	return opt_epsilon;
}

void compute_rbf_weights_aux( const int rbf_type,								//
							   	   const Scalar epsilon, 						//
								   const __global SizeType *cell_ptr, 			//
								   const __global SizeType *cell_index,			//
								   const __global SizeType *dims, 				//
								   const Scalar max_radius,						//
								   const __global Scalar *aabb,					//
								   const __global Scalar *points, 				//
								   const __global Scalar *function,				//
								   const __global SizeType *subdomain_ptr,		//
								   __global SizeType *subdomain_index,			//
								   __global Scalar *weights, 					//
								   __global Scalar *opt_epsilons,				//
								   MATRIX_COND_GLOBAL Scalar *matrix 			//
								   )		
{
	SizeType tensor_index [N_DIMS];
	SizeType p_dims		  [N_DIMS];
	SizeType three_dims   [N_DIMS];

	Scalar rhs[MAX_N_DOFS];
	Scalar p_weights[MAX_N_DOFS];


	const SizeType n_epsilons = N_EPSILONS;
	Scalar epsilons 		   [N_EPSILONS];
	epsilons[0] = epsilon;

	for(SizeType i = 1; i < n_epsilons; ++i) {
		epsilons[i] = i/(n_epsilons - 1.0)*(MAX_EPS-MIN_EPS)+MIN_EPS;
	}

	generic_set(MAX_N_DOFS, 0., p_weights);


	SizeType selected[MAX_N_DOFS];
	SizeType n_selected = 0;

	Scalar center[N_DIMS];

	Scalar aabb_min  [N_DIMS];
	Scalar aabb_range[N_DIMS];

	const Scalar max_radius_squared = max_radius*max_radius;

	generic_copy(N_DIMS, dims, p_dims);
	generic_copy(N_DIMS, aabb, aabb_min);
	loop_k(N_DIMS, aabb_range[k] = aabb[N_DIMS+k] - aabb_min[k]);

	SizeType total_dim = 3;
	loop_k(N_DIMS-1, total_dim *= 3);
	
	loop_k(N_DIMS, three_dims[k] = 3 );

	SizeType n_cells = 1;
	loop_k(N_DIMS, n_cells *= dims[k]);

	for(SizeType i = get_global_id(0); i < n_cells; i+= get_global_size(0)) {
		linear_index_to_tensor_index(p_dims, i, tensor_index);

		if(is_boundary(tensor_index, p_dims)) {
			continue;
		}

		cell_center(tensor_index, p_dims, aabb_min, aabb_range, center);

		n_selected = point_selection(cell_ptr, cell_index, points, 
			max_radius_squared, p_dims, 
			total_dim,  three_dims, tensor_index, center, selected);

		if(n_selected <= 0) continue;

		
		if(must_find_optimal_epsilon) {
			const Scalar opt_epsilon = find_optimal_epsilon_brent(rbf_type, n_selected, selected, 
				points, function, n_epsilons, epsilons, 
				matrix, rhs, p_weights);
			opt_epsilons[i] = opt_epsilon;

		} else {
			assemble_rbf_system(rbf_type, n_selected, selected, points, function, epsilon, matrix, rhs);
			ldl_solve_in_place(n_selected, matrix, rhs, p_weights);
			opt_epsilons[i] = epsilon;
		} 

		loop_k(n_selected, weights[subdomain_ptr[i] + k] = p_weights[k]);
		loop_k(n_selected, subdomain_index[subdomain_ptr[i] + k] = selected[k]);
	}
}


#endif