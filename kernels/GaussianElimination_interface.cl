#ifndef GAUSSIAN_ELIMINATION_INTERFACE_CL
#define GAUSSIAN_ELIMINATION_INTERFACE_CL 

#include "pum_KernelBase.cl"
#include "pum_Functions_interfaces.cl"

#ifndef CLIPP_HOST_CL	

void gauss_elim_solve(const SizeType n, Scalar *A, const Scalar *b, Scalar *x);
void gauss_elim_solve_global(const SizeType n, __global Scalar *A, const Scalar *b, Scalar *x);

#endif //CLIPP_HOST_CL

#endif //GAUSSIAN_ELIMINATION_INTERFACE_CL