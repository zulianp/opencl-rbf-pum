#ifndef QR_SOLVE_INPLACE_INTERFACE_CL			
#define QR_SOLVE_INPLACE_INTERFACE_CL

#include "pum_KernelBase.cl"
#include "pum_Functions_interfaces.cl"

#ifndef CLIPP_HOST_CL	

void qr_solve_inplace(const SizeType n, MATRIX_COND_GLOBAL Scalar *A, const Scalar *b, Scalar *x);

#endif //CLIPP_HOST_CL

#endif //QR_SOLVE_INPLACE_INTERFACE_CL