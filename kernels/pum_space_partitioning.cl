
#ifndef PUM_INDEX_SPACE_CL
#define PUM_INDEX_SPACE_CL

#include "pum_Functions.cl"

__kernel void compute_aabbs(const SizeType n_points, const __global Scalar *points, __local Scalar * local_work, __global Scalar *aabbs)
{
	Scalar p 	   	   [N_DIMS];
	Scalar aabb_min_max[2*N_DIMS];

	generic_set(N_DIMS,  LARGE_VALUE, aabb_min_max);
	generic_set(N_DIMS, -LARGE_VALUE, &aabb_min_max[N_DIMS]);

	for(SizeType i = get_global_id(0); i < n_points; i+= get_global_size(0)) {
		const SizeType offset = i*N_DIMS;
		generic_copy(N_DIMS, &points[offset], p);
		min_n(p,  aabb_min_max, aabb_min_max);
		max_n(p, &aabb_min_max[N_DIMS], &aabb_min_max[N_DIMS]);
	}


	reduce_aabbs(aabb_min_max, local_work, aabbs);

	//printf("ciao: %d, %g, %g, %g\n", N_DIMS, aabbs[N_DIMS+0], aabbs[N_DIMS+1], aabbs[N_DIMS+2]);
}

__kernel void find_cells(const SizeType n_points,
	const __global Scalar *points,
	const __global Scalar *aabb,
	const __global SizeType *dims,
	__global SizeType *indicator)
{
	Scalar p 	     [N_DIMS];
	Scalar aabb_min  [N_DIMS];
	Scalar aabb_range[N_DIMS];

	SizeType p_dims  [N_DIMS];

	generic_copy(N_DIMS, dims, p_dims);
	generic_copy(N_DIMS, aabb, aabb_min);
	loop_k(N_DIMS, aabb_range[k] = aabb[N_DIMS+k] - aabb_min[k]);

	// if(get_global_id(0) == 0) {
	// 	printf("[%g %g, %g %g]\n", aabb[0], aabb[1], aabb[2], aabb[3]);
	// }

	for(SizeType i = get_global_id(0); i < n_points; i+= get_global_size(0)) {
		const SizeType offset = i*N_DIMS;
		generic_copy(N_DIMS, &points[offset], p);

		const SizeType index = hash(p, aabb_min, aabb_range, p_dims);
		if(index == -1) {
			printf("error -> %d\n", i);
		}
		indicator[i] = index;
	}
}

__kernel void count_points_in_subdomain( const __global SizeType *cell_ptr, 	//0
										 const __global SizeType *cell_index,	//1
										 const __global SizeType *dims, 		//2
										 const Scalar max_radius,				//3
										 const __global Scalar *aabb,			//4
										 const __global Scalar *points, 		//5
										 __global SizeType *n_elements)			//6
{
	SizeType tensor_index[N_DIMS];
	SizeType p_dims		 [N_DIMS];
	SizeType three_dims   [N_DIMS];


	Scalar p 	 [N_DIMS];
	Scalar center[N_DIMS];

	Scalar aabb_min  [N_DIMS];
	Scalar aabb_range[N_DIMS];

	Scalar max_radius_squared = max_radius*max_radius;
	// Scalar max_radius_squared = 5;

	generic_copy(N_DIMS, dims, p_dims);
	generic_copy(N_DIMS, aabb, aabb_min);
	loop_k(N_DIMS, aabb_range[k] = aabb[N_DIMS+k] - aabb_min[k]);

	SizeType total_dim = 3;
	loop_k(N_DIMS-1, total_dim *= 3);

	loop_k(N_DIMS, three_dims[k] = 3 );

	SizeType n_cells = 1;
	loop_k(N_DIMS, n_cells *= dims[k]);

	for(SizeType i = get_global_id(0); i < n_cells; i+= get_global_size(0)) {
		linear_index_to_tensor_index(p_dims, i, tensor_index);

		if(is_boundary(tensor_index, p_dims)) {
			n_elements[i] = 0;
			continue;
		}

		SizeType p_n_elements = 0;

		loop_k(N_DIMS, center[k] = aabb_min[k] + (tensor_index[k] + 0.5) * aabb_range[k]/dims[k]);

		for(SizeType j = 0; j < total_dim; ++j) {
			const SizeType shifted_index =  shifted_index_from_tensor_index(three_dims, p_dims, j, tensor_index);

			const SizeType begin = cell_ptr[shifted_index];
			const SizeType end   = cell_ptr[shifted_index + 1];

			for(SizeType l = begin; l != end; ++l) {
				const SizeType point_index = cell_index[l];
				const SizeType point_offset = point_index * N_DIMS;

				generic_copy(N_DIMS, &points[point_offset], p);

				// Scalar dist = 0;
				// loop_k(N_DIMS, dist += (p[k] - center[k]) * (p[k] - center[k]) );

				if(distance_squared_d(p, center) < max_radius_squared) {
					++p_n_elements;
				}
			}
		}

		n_elements[i] = p_n_elements;
	}
}

__kernel void rearrange_points(const __global SizeType *dims,
						  const __global SizeType *cell_ptr, 			//2
						  __global SizeType *cell_index,			//3
						  const __global Scalar *points,
						  const __global Scalar *fun,
						  __global Scalar *rearranged,
						  __global Scalar *rearranged_fun
						  )
{

	SizeType n_cells = 1;
	loop_k(N_DIMS, n_cells *= dims[k]);

	for(SizeType i = get_global_id(0); i < n_cells; i+= get_global_size(0)) {

		const SizeType begin = cell_ptr[i];
		const SizeType end   = cell_ptr[i+1];


		for(SizeType j = begin; j < end; ++j) {
			const SizeType index = cell_index[j];
			loop_k(N_DIMS, rearranged[j * N_DIMS + k] = points[index * N_DIMS + k]);
			rearranged_fun[j] = fun[index];

			cell_index[j] = j;
		}
	}
}

#endif //PUM_INDEX_SPACE_CL

