#ifndef REDUCE_CL
#define REDUCE_CL

#include "pum_KernelBase.cl"


#define generic_reduce_private_values(n_private_values, stride, private_values, /*__local Scalar **/local_work, /*__global Scalar **/result, binary_operation)                                     \
{                                                                                                        \
      Scalar private_value[(stride)];                                 	                                 \
	  generic_copy((stride), private_values, private_value);      						                        \
      for(SizeType i = 1; i < (n_private_values); ++i) {                                                 \
      	   const SizeType offset = i*(stride);															               \
           binary_operation(private_value, (&private_values[offset]), private_value);                    \
      }                                                                                                  \
                                                                                                         \
      const SizeType l = get_local_id(0);                                                                \
      const SizeType localSize = get_local_size(0);                                                      \
      SizeType offset = localSize >> 1;                                                                  \
                                                                                                         \
      generic_copy((stride), private_values, &local_work[l*(stride)]);                                   \
      for(SizeType k = 0; offset > 0; ++k) {                                                             \
                                                                                                         \
            SizeType index = (k+offset) * (stride);                                                      \
            barrier(CLK_LOCAL_MEM_FENCE);                                                                \
            if(index < localSize) { generic_copy((stride), (&local_work[index]), private_value);  }      \
            barrier(CLK_LOCAL_MEM_FENCE);                                                                \
                                                                                                         \
            binary_operation(private_value, (&local_work[index]), (&local_work[index]));                 \
            offset >>= 1;                                                                                \
      }                                                                                                  \
                                                                                                         \
      if(l == 0) {                                                                                       \
      	const SizeType global_offset = get_group_id(0)*(stride);										            \
      	generic_copy((stride), &local_work[0], &result[global_offset]);			         				   \
      }																								                           \
}     





#endif //REDUCE_CL