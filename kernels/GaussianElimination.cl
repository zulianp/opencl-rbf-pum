#ifndef  GAUSSIAN_ELIMINATION_CL
#define GAUSSIAN_ELIMINATION_CL

#include "pum_KernelBase.cl"
#include "GaussianElimination_interface.cl"

//A in n x n
void gauss_elim_solve(const SizeType n, Scalar *A, const Scalar *b, Scalar *x)
{
    for(SizeType i = 0; i < n; ++i) {
        x[i] = b[i];
    }

    //elimination process starts
    for(SizeType i = 0; i < n-1; ++i) {
        SizeType p = i;

    //comparison to select the pivot
        for(SizeType j = i + 1; j < n; ++j) {
            if(fabs(value_at(n, j, i, A)) > fabs(value_at(n, i, i, A))) {
                matrix_swap_rows(n, i, j, A);
                generic_swap(Scalar, x[i], x[j]);
            }
        }

    //cheking for nullity of the pivots
        while(fabs(value_at(n, p, i, A)) < DEFAULT_TOLLERANCE && p < n ) { ++p; }

        if(p == n) {
            printf("gauss_elim_solve: No unique solution");
            break;
        } else  if(p != i) {
            matrix_swap_rows(n, i, p, A);
            generic_swap(Scalar, x[i], x[p]);
        }

        for(SizeType j = i+1; j < n; ++j) {
            const Scalar m = value_at(n, j, i, A)/value_at(n, i, i, A);
            
            for(SizeType k = i + 1; k < n; ++k) {
                value_at(n, j, k, A) -= m * value_at(n, i, k, A);
            }

            x[j] -= m * x[i]; 
        }
    }

//checking for nonzero of last entry
    if (fabs(value_at(n, n-1, n-1, A)) < DEFAULT_TOLLERANCE) {
        printf("gauss_elim_solve: No unique solution");
        return;
    }

//backward substitution
    x[n - 1] /= value_at(n, n-1, n-1, A); 


    for(SizeType i = n - 2; i>= 0; --i) {
        Scalar sumax = 0;
        for(SizeType j = i+1; j < n; ++j) {
            sumax += value_at(n, i,j, A) * x[j];
        }

        x[i] = (x[i] - sumax)/value_at(n, i, i, A);
    }
}

void gauss_elim_solve_global(const SizeType n, __global Scalar *A, const Scalar *b, Scalar *x)
{
    for(SizeType i = 0; i < n; ++i) {
        x[i] = b[i];
    }

    //elimination process starts
    for(SizeType i = 0; i < n-1; ++i) {
        SizeType p = i;

    //comparison to select the pivot
        for(SizeType j = i + 1; j < n; ++j) {
            if(fabs(value_at(n, j, i, A)) > fabs(value_at(n, i, i, A))) {
                matrix_swap_rows_global(n, i, j, A);
                generic_swap(Scalar, x[i], x[j]);
            }
        }

    //cheking for nullity of the pivots
        while(fabs(value_at(n, p, i, A)) < DEFAULT_TOLLERANCE && p < n ) { ++p; }

        if(p == n) {
            printf("gauss_elim_solve: No unique solution");
            break;
        } else  if(p != i) {
            matrix_swap_rows_global(n, i, p, A);
            generic_swap(Scalar, x[i], x[p]);
        }

        for(SizeType j = i+1; j < n; ++j) {
            const Scalar m = value_at(n, j, i, A)/value_at(n, i, i, A);
            
            for(SizeType k = i + 1; k < n; ++k) {
                value_at(n, j, k, A) -= m * value_at(n, i, k, A);
            }

            x[j] -= m * x[i]; 
        }
    }

//checking for nonzero of last entry
    if (fabs(value_at(n, n-1, n-1, A)) < DEFAULT_TOLLERANCE) {
        printf("gauss_elim_solve: No unique solution");
        return;
    }

//backward substitution
    x[n - 1] /= value_at(n, n-1, n-1, A); 


    for(SizeType i = n - 2; i>= 0; --i) {
        Scalar sumax = 0;
        for(SizeType j = i+1; j < n; ++j) {
            sumax += value_at(n, i,j, A) * x[j];
        }

        x[i] = (x[i] - sumax)/value_at(n, i, i, A);
    }
}

#endif //GAUSSIAN_ELIMINATION_CL
