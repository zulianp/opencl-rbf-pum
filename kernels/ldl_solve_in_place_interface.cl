#ifndef LDL_SOLVE_IN_PLACE_INTERFACE_CL
#define LDL_SOLVE_IN_PLACE_INTERFACE_CL

#include "pum_KernelBase.cl"
#include "pum_Functions_interfaces.cl"


#ifndef CLIPP_HOST_CL	

void ldl_decomposition_in_place(const SizeType n, MATRIX_COND_GLOBAL Scalar *A, Scalar *d);
void ldl_solve_in_place(const SizeType n, MATRIX_COND_GLOBAL Scalar *A, const Scalar *b, Scalar *x);
void ldl_solve(const SizeType n, MATRIX_COND_GLOBAL Scalar *ldl_L,  const Scalar *ldl_diag, const Scalar *b, Scalar *x);

//ldl_inv_L can be ldl_L
void ldl_inverse_transposed_of_lower_tri(const SizeType n, const MATRIX_COND_GLOBAL Scalar *ldl_L,MATRIX_COND_GLOBAL Scalar *ldl_inv_L);
void ldl_diagonal_of_inverse(const SizeType n, const MATRIX_COND_GLOBAL Scalar *ldl_L_and_inv_L, const Scalar *ldl_diag, Scalar *diag_of_inverse);

#endif //CLIPP_HOST_CL

#endif //LDL_SOLVE_IN_PLACE_INTERFACE_CL