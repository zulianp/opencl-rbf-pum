#pragma once

#include <vector>
#include <string>

namespace rbf_pum
{
	typedef double Scalar;
	// typedef float Scalar;
	typedef int SizeType;

	struct RBFData
	{
		std::vector<Scalar> opt_epsilons;
		std::vector<SizeType> cell_ptr;
		std::vector<SizeType> cell_index;
		std::vector<SizeType> dims;
		Scalar max_radius;
		std::vector<Scalar> aabb;
		std::vector<Scalar> rearranged_points;
		std::vector<Scalar> rearranged_function;
		std::vector<SizeType> subdomain_ptr;
		std::vector<SizeType> subdomain_index;
		std::vector<Scalar> weights;
		SizeType dim;
		SizeType n_cells;
		std::string flags;
	};

	/**
	* points where the function is define
	* function values to interpolate
	* verbose level 0=no ouput, 1=times, 2=all
	* rbf to use GA, IMQ, M2, M4, M6, W2, W4, W6
	* otpimize eps
	* unit_cube data is in unit cube
	* max number of thread (-1 is default)
	*/
	int init(const std::vector<Scalar> &points, const std::vector<Scalar> &function, RBFData &data,
		int verbose = 0,
		const std::string &rbf = "M4",
		bool opt = false,
		bool unit_cube  = false,
		int num_threads = -1
		);

	/**
	* eval_points evaluation points
	* resul interpolated values
	* verbose level 0=no ouput, 1=times, 2=all
	* rbf to use GA, IMQ, M2, M4, M6, W2, W4, W6
	* otpimize eps
	* unit_cube data is in unit cube
	* max number of thread (-1 is default)
	* oracle to compute error
	*/
	int interpolate(const RBFData &data, const std::vector<Scalar> &eval_points, std::vector<Scalar> &result,
		int verbose = 0,
		const std::string &rbf = "M4",
		bool opt = false,
		bool unit_cube  = false,
		int num_threads = -1,
		const std::vector<Scalar> oracle = std::vector<Scalar>()
		);
}