# README #

This code accompanies the paper:
OpenCL based parallel algorithm for RBF-PUM interpolation

# Installation
For running the python script you need to install **pyopencl** (https://pypi.python.org/pypi/pyopencl) which can be done with **pip** (https://docs.python.org/3/installing/).


For compiling the cpp code the CPU opencl SDK is required (https://software.intel.com/en-us/intel-opencl or http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk/)

To compile:

* Linux: g++ rbf_interpolate.cpp -O3 -lOpenCL -I<path_to_opencl_include_folder> -L<path_to_opencl_lib_folder> -o interp
* MacOS: g++ -g rbf_interpolate.cpp -framework OpenCL -o interp

# Running the interpolator
The script rbf_interpolate.py and the cpp executable *interp* take 4 parameters

1. The data points file
1. The function file
1. The evaluation points file
1. The path of the output file 

Additionally optional paramters can be passed in the tail of the command (C++ only with abbreviated version). Here is the list:

* --rbf (-r) GA (gaussian), IMQ (Inverse multi-quadric), M2, M4, M6 (Matern functions), and W2, W4, W6 (Wendland)
* --num_threads (-p) number of cores used
* --unit_cube (-u) assumues that the data-points are contained in the unit-cube
* --oracle (-o) file containing the exact solution at the evaluation points for computing the interpolation error.
* --opt (-t) run the LOCCV optimization algorithm for selecting the locally optimal epsilons


For running the python script with our example data, type: 

```
#!bash

python rbf_interpolate.py data/points.txt data/fun.txt data/eval_points.txt output.txt -o data/oracle.txt
```

For running the C++ executable with our example data, type:

```
#!bash

./interp data/points.txt data/fun.txt data/eval_points.txt output.txt -o data/oracle.txt
```