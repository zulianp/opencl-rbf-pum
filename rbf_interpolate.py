#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function
import numpy as np
import pyopencl as cl
from optparse import OptionParser
import sys
import time
import math

size_type = np.int32
real = np.float64
tol = 0

if(real().nbytes == 4):
    prec_str = ' -DUSE_SINGLE_PRECISION=1'
    tol = 1e-6
else:
    prec_str = ' -DUSE_DOUBLE_PRECISION=1'
    tol = 1e-8


def error_usage():
    print("[Error] Wrong number of parameters")

def print_usage():
    print('usage: rbf_interpolate.py <points.txt> <function.txt> <eval_points.txt> <interpolated_function.txt>')


def rbf_type_from_string(rbf_string):
    cases = { 
    'GA' : 0, 
    'IMQ' : 1,
    'M2' : 2,
    'M4' : 3,
    'M6' : 4,
    'W2' : 5,
    'W4' : 6,
    'W6' : 7
     }
    return size_type(cases[rbf_string])


def num(s):
    try:
        return size_type(s)
    except ValueError:
        return real(s)

def get_preferred_epsilon(rbf_type):
    eps = [ 2.0,                                  #GA
            2.0,                                  #IMQ
            2.0,  2.0,  2.0,                      #Mx
            0.01, 0.01, 0.01                      #Wx
    ]
    return real(eps[rbf_type])  

#"<rbf> <points.txt> <function.txt> <eval_points.txt> <interpolated_function.txt>
parser = OptionParser()
parser.add_option("-r", "--rbf", dest="rbf",
                  help="The type of rbf", type="string", default="M4")

# parser.add_option("-s", "--serial", dest="run_serial",
#                   help="set to false if serial exec", type="string", default="false")

parser.add_option("-p", "--num_threads", dest="num_threads",
                  help="number of thrad used", type="int", default="-1")

parser.add_option("-u", "--unit_cube", dest="unit_cube",
                  help="true if is [0, 1]^dim", type="string", default="false")

parser.add_option("-o", "--oracle", dest="oracle",
                  help="oracle path", type="string", default=None)

parser.add_option("-t", "--opt", dest="opt",
                  help="optimize local epsilons", type="string", default="true")


true_string = ['True', 'true', '1']

(options, args) = parser.parse_args()

# print(args)
# print(options)

if len(args) < 4:
    error_usage();
    print_usage()
    exit()


points      = np.fromfile(args[0], dtype=real, sep=' ')
function    = np.fromfile(args[1], dtype=real, sep=' ')
eval_points = np.fromfile(args[2], dtype=real, sep=' ')
output_path = args[3]

dim         = size_type(len(points)/len(function))

script_time = time.time()

#read programs
isect_file = open('kernels/pum_space_partitioning.cl', 'r')
isect_prg_str = isect_file.read()

rbf_file = open('kernels/pum_RBF.cl', 'r')
rbf_prg_str = rbf_file.read()

# create opencl context and queue
ctx = cl.create_some_context()
queue = cl.CommandQueue(ctx)

if options.opt in true_string:
    opt_eps_str = ' -Dmust_find_optimal_epsilon=1'
else:
    opt_eps_str = ' -Dmust_find_optimal_epsilon=0'

# build programs
isect_prg = cl.Program(ctx, isect_prg_str).build('-I./kernels -DN_DIMS=' + str(dim) + ' ' + prec_str)


#init input read-only buffers
mf = cl.mem_flags
b_points = cl.Buffer(ctx, mf.READ_ONLY | mf.USE_HOST_PTR, hostbuf=points)
b_function = cl.Buffer(ctx, mf.READ_ONLY | mf.USE_HOST_PTR, hostbuf=function)
b_eval_points = cl.Buffer(ctx, mf.READ_ONLY | mf.USE_HOST_PTR, hostbuf=eval_points)

rbf_type = rbf_type_from_string(options.rbf)
eps = get_preferred_epsilon(rbf_type)


n_points = size_type(len(points)/dim)
n_subdomains = size_type(np.ceil(0.5 * pow(n_points * 0.5, 1.0/dim)))
print("npu =", n_subdomains)

total_time = 0.

#FIXME does not work properly
# max_group_size = ctx.devices[0].get_info(cl.device_info.MAX_WORK_GROUP_SIZE)
# group_size_multiple = isect_prg.compute_aabbs.get_work_group_info(cl.kernel_work_group_info.PREFERRED_WORK_GROUP_SIZE_MULTIPLE, ctx.devices[0])

# print("max_work_group_size:", max_group_size)
# print("group_size_multiple:", group_size_multiple)

device_type = ctx.devices[0].get_info(cl.device_info.TYPE)

# if device_type == cl.device_type.GPU:
#     group_size = size_type(min(max(math.floor(n_points/group_size_multiple), 1) * group_size_multiple, max_group_size))
# else:
group_size = size_type(1)

# group_size = size_type(1)

n_groups = size_type(n_points/group_size)
global_size = n_points

print("-----------------------------")
print("device:", ctx.devices[0].get_info(cl.device_info.NAME))
print("group_size:", group_size)
print("n_groups:", n_groups)
print("global_size:", global_size)
print("-----------------------------")

if options.num_threads > 0:
    group_size = 1
    global_size = options.num_threads
    n_groups = 1


aabb = np.zeros([dim * 2], dtype=real)

if options.unit_cube in true_string:
    # fill max coords
    for j in range(0, dim):
        aabb[dim+j] = 1
else:
    ####################################################################################
    ####################################################################################
    #############################Compute aabbs##########################################
    start = time.time()

    aabbs = np.zeros([n_groups * 2 * dim], dtype=real)
    b_aabbs = cl.Buffer(ctx, mf.WRITE_ONLY | mf.USE_HOST_PTR, hostbuf=aabbs)

    isect_prg.compute_aabbs(queue, [global_size], [group_size],
                            n_points, b_points, cl.LocalMemory(real().nbytes * group_size * 2 * dim), b_aabbs)

    # reads the content for completing the reduction and obtaining aaabb
    cl.enqueue_map_buffer(queue, b_aabbs, cl.map_flags.READ, 0, len(aabbs), dtype=real)

    # even if the previous call is blocking lets just empty the queue
    queue.finish()

    for j in range(0, dim):
        aabb[j] = aabbs[j]
        aabb[dim+j] = aabbs[dim+j]

    for i in range(1, n_groups):
        offset = i*2
        for j in range(0, dim):
            aabb[j] = min(aabbs[dim*offset + j], aabb[j])
            aabb[dim+j] = max(aabbs[(offset+1)*dim + j], aabb[dim+j])

    end = time.time()
    total_time += end - start
    print('compute_aabbs: ' + str(end - start))

# end if options.unit_cube in true_string:

aabb_ranges = np.zeros(dim, dtype=real)
for j in range(0, dim):
    aabb_ranges[j] = aabb[j+dim] - aabb[j]

aabb_ranges_min = min(aabb_ranges)
n_cells_x_dim   = np.zeros(dim, dtype=size_type)

total_npu = 1

for i in range(0, dim):
    n_cells_x_dim[i] = n_subdomains * (aabb_ranges[i]/aabb_ranges_min)
    total_npu *= n_cells_x_dim[i]

print("total_npu =", total_npu)

for j in range(0, dim):
    aabb[j] -= tol
    aabb[j + dim] += tol

h = (aabb[range(dim, dim * 2)] - aabb[range(0, dim)])/n_cells_x_dim
aabb[range(dim, dim * 2)] += h
aabb[range(0, dim)] -= h
dims = n_cells_x_dim + 2

#################################################################
#################### Find cells #############################

start = time.time()
b_aabb = cl.Buffer(ctx, mf.READ_ONLY | mf.USE_HOST_PTR, hostbuf=aabb)
b_dims = cl.Buffer(ctx, mf.READ_ONLY | mf.USE_HOST_PTR, hostbuf=dims)

indicator = np.zeros([n_points], dtype=size_type)
b_indicator = cl.Buffer(ctx, mf.WRITE_ONLY | mf.USE_HOST_PTR, hostbuf=indicator)
isect_prg.find_cells(queue, [global_size], None, n_points, b_points, b_aabb, b_dims, b_indicator)
cl.enqueue_map_buffer(queue, b_indicator, cl.map_flags.READ, 0, len(indicator), dtype=real)
queue.finish()

end = time.time()
total_time += end - start
print("find_cells:", (end - start))



#################################################################
########################## count points in cells ################

n_cells = np.multiply.reduce(dims)
cell_count = np.zeros([n_cells], dtype=size_type)

for i in range(0, len(indicator)):
    cell_count[indicator[i]] += 1


#################################################################
########################## fill cells ###########################

start = time.time()
cell_ptr = np.zeros([len(cell_count) + 1], dtype=size_type)

for i in range(0, len(cell_count)):
    cell_ptr[i+1] = cell_ptr[i] + cell_count[i]

inserted = np.zeros([len(cell_count)], dtype=size_type)
cell_index = np.zeros([len(indicator)], dtype=size_type)

for i in range(0, len(indicator)):
    ind = indicator[i]
    cell_index[cell_ptr[ind] + inserted[ind]] = i
    inserted[ind] += 1

end = time.time()
total_time += end - start
print("fill_cells:", (end - start))

#################################################################
#################### rearrange points ###########################

start = time.time()
b_cell_ptr = cl.Buffer(ctx, mf.READ_ONLY | mf.USE_HOST_PTR, hostbuf=cell_ptr)
b_cell_index = cl.Buffer(ctx, mf.READ_ONLY | mf.USE_HOST_PTR, hostbuf=cell_index)

b_temp_points = cl.Buffer(ctx, mf.WRITE_ONLY, len(points) * real().nbytes)
b_temp_function = cl.Buffer(ctx, mf.WRITE_ONLY, len(function) * real().nbytes)

n_global = len(cell_ptr)-1
if options.num_threads > 0:
    n_global = options.num_threads



isect_prg.rearrange_points(queue, [n_global], None,
                           b_dims, b_cell_ptr, b_cell_index, b_points, b_function,
                           b_temp_points, b_temp_function)
queue.finish()


b_points = b_temp_points
b_function = b_temp_function

end = time.time()
total_time += end - start
print("rearrange points:", end - start)


#################################################################
#################### count points in subdomain ##################
start = time.time()
max_radius = np.sqrt(2)/(min(n_cells_x_dim)) * aabb_ranges_min

n_global = n_cells
if options.num_threads > 0:
    n_global = options.num_threads

n_elements = np.zeros([n_cells],dtype=size_type)
b_n_elements = cl.Buffer(ctx, mf.READ_WRITE | mf.USE_HOST_PTR, hostbuf=n_elements)

isect_prg.count_points_in_subdomain(queue, [n_global], None,
                                    b_cell_ptr, b_cell_index, b_dims, max_radius,
                                    b_aabb, b_points, b_n_elements)
cl.enqueue_map_buffer(queue, b_n_elements, cl.map_flags.READ, 0, len(n_elements), dtype=real)                               
queue.finish()                                    

end = time.time()
total_time += end - start
print("count points in subdomain:", end - start)

#################################################################
#################### compute rbf weights  #######################

max_n_dofs = np.max(n_elements)
assert(max_n_dofs > 0)

# rbf_flags = " -DN_EPSILONS=" + options.n_epsilons + ' -DMAX_N_DOFS=' + str(max_n_dofs) + opt_eps_str
rbf_flags = ' -DMAX_N_DOFS=' + str(max_n_dofs) + opt_eps_str
use_dynamic_mem = dim > 2

if use_dynamic_mem:
    rbf_flags += " -DUSE_GLOBAL_MATRIX"


rbf_prg = cl.Program(ctx, rbf_prg_str).build('-I./kernels -DN_DIMS=' + str(dim) + prec_str + rbf_flags)

subdomain_ptr = np.zeros(len(n_elements) + 1, dtype=size_type)
for i in range(0, len(n_elements)):
    subdomain_ptr[i+1] = subdomain_ptr[i] + n_elements[i]

n_weights = subdomain_ptr[len(n_elements)]

b_subdomain_ptr = cl.Buffer(ctx, mf.READ_ONLY | mf.USE_HOST_PTR, hostbuf=subdomain_ptr)
b_subdomain_index = cl.Buffer(ctx, mf.READ_WRITE, n_weights * size_type().nbytes)
b_weights = cl.Buffer(ctx, mf.READ_WRITE, n_weights * real().nbytes)
b_opt_epsilons = cl.Buffer(ctx, mf.READ_WRITE, n_cells * real().nbytes)
 
start = time.time()

if use_dynamic_mem:
    n_global = 1
    if options.num_threads > 0:
        n_global = options.num_threads
    else:
        n_global = ctx.devices[0].max_compute_units

    b_matrix = cl.Buffer(ctx, mf.WRITE_ONLY, n_global * max_n_dofs * max_n_dofs * real().nbytes)

    rbf_prg.compute_rbf_weights_dynamic(queue, [n_global], None,
                                        rbf_type, eps, b_cell_ptr, b_cell_index, b_dims,
                                        max_radius, b_aabb, b_points, b_function,
                                        b_subdomain_ptr, b_subdomain_index, b_weights,
                                        b_opt_epsilons,
                                        b_matrix)
    queue.finish()

else:
    n_global = 1
    if options.num_threads > 0:
        n_global = options.num_threads
    else:
        n_global = n_cells

    rbf_prg.compute_rbf_weights(queue, [n_global], None,
                                rbf_type, eps, b_cell_ptr, b_cell_index, b_dims, max_radius,
                                b_aabb, b_points, b_function,
                                b_subdomain_ptr, b_subdomain_index, b_weights,
                                b_opt_epsilons)
    queue.finish()

end = time.time()
total_time += end - start
print("compute rbf weights:", end - start)

#################################################################
#################### interpolate  #######################

start = time.time()

n_eval_points = size_type(len(eval_points)/dim)
n_global = n_eval_points
if options.num_threads > 0:
    n_global = options.num_threads

#init 1D output function
result = np.zeros(n_eval_points, dtype=real)
b_result = cl.Buffer(ctx, mf.WRITE_ONLY | mf.USE_HOST_PTR, hostbuf=result)

rbf_prg.rbf_interpolate(queue, [n_global], None,
                        rbf_type, b_opt_epsilons, b_cell_ptr, b_cell_index, b_dims,
                        max_radius, b_aabb, b_points, b_function,
                        b_subdomain_ptr, b_subdomain_index, b_weights,
                        n_eval_points, b_eval_points, b_result)

cl.enqueue_map_buffer(queue, b_result, cl.map_flags.READ, 0, len(result), dtype=real)
queue.finish()

end = time.time()
total_time += end - start
print("interpolate:", end - start)

script_time = time.time() - script_time


if options.oracle:
    oracle = np.fromfile(options.oracle, dtype=real, sep=' ')
    print('rmse_err =', np.linalg.norm(oracle - result) / np.sqrt(len(oracle)))
    print('max_err =', max(abs(oracle - result)))


result.tofile(output_path, sep="\n")

print("total_time:", total_time)
print("script time:", script_time)
