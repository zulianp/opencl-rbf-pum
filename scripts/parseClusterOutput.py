import os
import glob


csv = open("scaling.csv","w")
csv.write("nThreads,npu,max_work_group_size,group_size_multiple,group_size,n_groups,global_size,total_npu,max_n_dofs,rmse_err,max_err,	compute_aabbs,find_cells,fill_cells,rearrange_points,count_points_in_subdomain,compute_rbf_weights,interpolate,total_time,script_time\n")


for output in glob.glob("./output3d/pum_*.txt"):
	(path, name) = os.path.split(output)
	nThreads=name.replace("pum_","")
	nThreads=nThreads.replace(".txt","")

	file=open(output,"r")


	npu = 0
	max_work_group_size = 0
	group_size_multiple = 0
	group_size = 0
	n_groups = 0
	global_size = 0
	total_npu = 0
	max_n_dofs = 0
	rmse_err = 0
	max_err = 0

	compute_aabbs = 0
	find_cells = 0
	fill_cells = 0
	rearrange_points = 0
	count_points_in_subdomain = 0
	compute_rbf_weights = 0
	interpolate = 0
	total_time = 0
	script_time = 0

	for line in file:
		if line.startswith("npu: "):
			npu = int(line.replace("npu: ", ""))

		elif line.startswith("max_work_group_size: "):
			max_work_group_size = int(line.replace("max_work_group_size: ", ""))

		elif line.startswith("group_size_multiple: "):
			group_size_multiple = int(line.replace("group_size_multiple: ", ""))

		elif line.startswith("group_size: "):
			group_size = int(line.replace("group_size: ", ""))

		elif line.startswith("n_groups: "):
			n_groups = int(line.replace("n_groups: ", ""))

		elif line.startswith("global_size: "):
			global_size = int(line.replace("global_size: ", ""))

		elif line.startswith("total_npu: "):
			total_npu = int(line.replace("total_npu: ", ""))

		elif line.startswith("max_n_dofs: "):
			max_n_dofs = int(line.replace("max_n_dofs: ", ""))

		elif line.startswith("rmse_err: "):
			rmse_err = float(line.replace("rmse_err: ", ""))

		elif line.startswith("max_err: "):
			max_err = float(line.replace("max_err: ", ""))


		elif line.startswith("compute_aabbs: "):
			compute_aabbs = float(line.replace("compute_aabbs: ", "").replace(" seconds", ""))

		elif line.startswith("find_cells: "):
			find_cells = float(line.replace("find_cells: ", "").replace(" seconds", ""))

		elif line.startswith("fill_cells: "):
			fill_cells = float(line.replace("fill_cells: ", "").replace(" seconds", ""))

		elif line.startswith("rearrange points: "):
			rearrange_points = float(line.replace("rearrange points: ", "").replace(" seconds", ""))

		elif line.startswith("count_points_in_subdomain: "):
			count_points_in_subdomain = float(line.replace("count_points_in_subdomain: ", "").replace(" seconds", ""))

		elif line.startswith("compute rbf weights: "):
			compute_rbf_weights = float(line.replace("compute rbf weights: ", "").replace(" seconds", ""))

		elif line.startswith("interpolate: "):
			interpolate = float(line.replace("interpolate: ", "").replace(" seconds", ""))

		elif line.startswith("total_time: "):
			total_time = float(line.replace("total_time: ", "").replace(" seconds", ""))

		elif line.startswith("script time: "):
			script_time = float(line.replace("script time: ", "").replace(" seconds", ""))

	csv.write(str(nThreads) + "," + str(npu) + "," + str(max_work_group_size) + "," + str(group_size_multiple) + "," + str(group_size) + "," + str(n_groups) + "," + str(global_size) + "," + str(total_npu) + "," + str(max_n_dofs) + "," + str(rmse_err) + "," + str(max_err) + "," + 	str(compute_aabbs) + "," + str(find_cells) + "," + str(fill_cells) + "," + str(rearrange_points) + "," + str(count_points_in_subdomain) + "," + str(compute_rbf_weights) + "," + str(interpolate) + "," + str(total_time) + "," + str(script_time) + "\n")


	file.close()

csv.close()