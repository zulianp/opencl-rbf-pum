#!/bin/bash -l
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --ntasks-per-core=1
#SBATCH --cpus-per-task=18
#SBATCH --time=00:30:00
#SBATCH --constraint=mc
#SBATCH -C mc

p=$SCRATCH/opencl-rbf-pum/data
module load slurm
module load daint-mc

srun -C mc ./interp $p/points.txt  $p/fun.txt $p/eval_points.txt interpolated_function.txt -o $p/oracle.txt -u 1
