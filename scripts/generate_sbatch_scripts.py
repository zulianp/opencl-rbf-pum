
import os

settings  = "#!/bin/bash -l\n"
settings += "#SBATCH --nodes=1\n"
settings += "#SBATCH --ntasks-per-node=1\n"
settings += "#SBATCH --ntasks-per-core=1\n"
settings += "#SBATCH --time=00:30:00\n"
settings += "#SBATCH --constraint=mc\n"
#settings += "#SBATCH -C mc\n"
output = "#SBATCH --output=./output/pum_%d.txt\n"
cpus = "#SBATCH --cpus-per-task=%d\n\n"

#command = "p=$SCRATCH/opencl-rbf-pum/data\n"
#command = "p=$SCRATCH/opencl-rbf-pum/data/2D\n"
command = "p=$SCRATCH/opencl-rbf-pum/data/3D\n"

command += "module load slurm\n"
command += "module load daint-mc\n"
command += "srun -C mc $SCRATCH/opencl-rbf-pum/interp $p/points.txt  $p/fun.txt $p/eval_points.txt interpolated_function.txt -r M4 -t 0 -o $p/oracle.txt -u 1 -p %d\n\n"

n_threads=range(1, 36, 2)

for t in n_threads:
    script = settings + (output % (t) ) + (cpus % (t)) + (command % (t)) 
    file = open("run_pum_%d.sbatch" % (t), "w")
    file.write(script)
    file.close()
    os.system("cat run_pum_%d.sbatch" % (t))


